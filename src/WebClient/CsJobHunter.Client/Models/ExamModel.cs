﻿using System;
using System.Collections.Generic;

namespace CsJobHunter.Client.Models
{
    public class ExamModel
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public string StudentFio { get; set; }

        public DateTime DateTimeBegin { get; set; }
        public DateTime? DateTimeEnd { get; set; }
        public bool IsFinished { get; set; }
        public bool IsPassed { get; set; }
        public int QuestionsCount { get; set; }
        public int PassedQuestionsCount { get; set; }

        public List<string> EmailAddresses { get; set; }
    }
}
