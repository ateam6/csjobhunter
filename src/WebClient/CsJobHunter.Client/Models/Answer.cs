﻿namespace CsJobHunter.Client.Models.Exams
{
    public class Answer
    {
        public long Id { get; set; }
        public string Text { get; set; }
    }
}
