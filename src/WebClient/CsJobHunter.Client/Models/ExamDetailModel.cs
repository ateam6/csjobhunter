﻿using System.Collections.Generic;
using CsJobHunter.Client.Models.Exams;

namespace CsJobHunter.Client.Models

{
    public class ExamDetailModel 
    {
        public long Id { get; set; }
        public long ExamId { get; set; }
        public int SerialNumber { get; set; }
        public int RemainCount { get; set; }
        public string QuestionText { get; set; }
        public int QuestionId { get; set; }
        public List<Answer> Answers { get; set; }
    }
}