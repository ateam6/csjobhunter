﻿namespace CsJobHunter.Client.Models
{
    public class UpdateExamDetailModel
    {
        public long Id { get; set; }
        public bool IsFinished { get; set; }
        public long ExamId { get; set; }
        public long QuestionId { get; set; }
        public int AnswerId { get; set; }
    }
}
