﻿namespace CsJobHunter.Client.Models
{
    public class StudentModel
    {
        public long Id { get; set; }
        public string FullName { get; set; }
    }
}
