using CsJobHunter.Client.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Blazor;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CsJobHunter.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri("http://localhost:5000/") });
            builder.Services.AddSyncfusionBlazor();
            builder.Services.AddTransient<IExamDetailsService, ExamDetailsService>();
            builder.Services.AddTransient<IExamService, ExamService>();

            await builder.Build().RunAsync();
        }
    }
}
