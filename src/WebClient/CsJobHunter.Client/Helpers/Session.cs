﻿using CsJobHunter.Client.Models;

namespace CsJobHunter.Client.Helpers
{
    public class Session
    {
        public static StudentModel GetCurrentUser()
        {
            var student = new StudentModel()
            {
                Id = 1,
                FullName = "Роберт Мартинов"
            };

            return student;
        }
    }
}
