﻿using CsJobHunter.Client.Models;
using System.Threading.Tasks;

namespace CsJobHunter.Client.Services
{
    public interface IExamDetailsService
    {
        //Task<IEnumerable<ExamWithDetailResponse>> GetAllByExamIdAsync(long examId);
        //Task<ExamWithDetailResponse> GetByIdAsync(long id);
        Task<ExamDetailModel> GetNextByExamIdAsync(long examId);
        //Task<bool> CreateAsync(CreateExamDetailsRequest createExamDetailsRequest);
        Task<bool> UpdateAsync(UpdateExamDetailModel updateExamDetailRequest);
    }
}
