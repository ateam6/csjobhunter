﻿using CsJobHunter.Client.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using CsJobHunter.Client.Helpers;

namespace CsJobHunter.Client.Services
{
    public class ExamService : IExamService
    {
        private readonly HttpClient _httpClient;

        public ExamService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<long> CreateAsync(CreateExamModel createExamModel)
        {
            var response = await _httpClient.PostAsJsonAsync($"exams", createExamModel);
            return await response.Content.ReadFromJsonAsync<long>();
        }

        public async Task<ExamModel> GetByIdAsync(long id)
        {
            return await _httpClient.GetFromJsonAsync<ExamModel>($"exams/{id}");
        }

        public async Task<List<ExamModel>> GetAllByStudentIdAsync(long studentId)
        {
            return await _httpClient.GetFromJsonAsync<List<ExamModel>>($"exams/students/{studentId}");
        }

        public async Task FinishAsync(long id)
        {
            var examModel = new ExamModel()
            {
                Id = id,
                IsFinished = true,
                StudentId = Session.GetCurrentUser().Id
            };

            await _httpClient.PutAsJsonAsync<ExamModel>($"exams", examModel);
        }
    }
}
