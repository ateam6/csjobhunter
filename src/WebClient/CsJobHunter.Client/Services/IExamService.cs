﻿using CsJobHunter.Client.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsJobHunter.Client.Services
{
    public interface IExamService
    {
        Task<long> CreateAsync(CreateExamModel createExamModel);
        Task<ExamModel> GetByIdAsync(long id);
        Task<List<ExamModel>> GetAllByStudentIdAsync(long studentId);
        Task FinishAsync(long id);
    }
}
