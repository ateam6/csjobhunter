﻿using CsJobHunter.Client.Models;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CsJobHunter.Client.Services
{
    public class ExamDetailsService : IExamDetailsService
    {
        private readonly HttpClient _httpClient;

        public ExamDetailsService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<ExamDetailModel> GetNextByExamIdAsync(long examId)
        {
            return await _httpClient.GetFromJsonAsync<ExamDetailModel>($"examdetails/next/{examId}");
        }

        public async Task<bool> UpdateAsync(UpdateExamDetailModel updateExamDetailRequest)
        {
            var response = await _httpClient.PutAsJsonAsync<UpdateExamDetailModel>($"examdetails", updateExamDetailRequest);
            return await response.Content.ReadFromJsonAsync<bool>();
        }
    }
}
