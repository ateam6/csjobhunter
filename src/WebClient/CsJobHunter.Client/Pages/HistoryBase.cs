﻿using CsJobHunter.Client.Helpers;
using CsJobHunter.Client.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsJobHunter.Client.Pages
{
    public class HistoryBase : ComponentBase
    {
        [Inject]
        public IExamService ExamService { get; set; }

        protected string FullName;

        protected List<Models.ExamModel> ExamResponses = new();

        protected override async Task OnInitializedAsync()
        {
            var id = Session.GetCurrentUser().Id;
            FullName = Session.GetCurrentUser().FullName;
            ExamResponses = await ExamService.GetAllByStudentIdAsync(id);
        }

        protected string GetStatus(Models.ExamModel examResponse)
        {
            return examResponse.IsFinished ? "Завершен" : "Не завершен";
        }

        protected string GetResult(Models.ExamModel examResponse)
        {
            return examResponse.IsPassed ? "Сдан" : "Не сдан";
        }

        protected string GetDuration(Models.ExamModel examResponse)
        {
            if (examResponse.DateTimeEnd is null)
                return "";

            TimeSpan timeSpan = ((DateTime)examResponse.DateTimeEnd).Subtract(examResponse.DateTimeBegin);

            return timeSpan.ToString(@"mm\:ss");
        }
    }
}
