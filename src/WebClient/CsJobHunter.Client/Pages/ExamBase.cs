﻿using System;
using System.Threading;
using CsJobHunter.Client.Helpers;
using CsJobHunter.Client.Models;
using CsJobHunter.Client.Services;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;

namespace CsJobHunter.Client.Pages
{
    public class ExamBase : ComponentBase
    {
        [Inject] public IExamService ExamService { get; set; }
        [Inject] public IExamDetailsService ExamDetailsService { get; set; }

        private static System.Timers.Timer aTimer;
        private static readonly int _counter = 60;

        private long currentExamId;
        protected ExamModel Exam { get; set; }
        protected string RadioIdValue;

        protected string QuestionsCount;
        private int RemainsCount;
        protected string QuestionNumber;
        protected bool IsShowResult;
        protected int Counter;


        protected ExamDetailModel GetNextResponse { get; set; }
        protected bool DisableStart { get; set; }
        protected bool DisableNext { get; set; }

        protected override async Task OnInitializedAsync()
        {
            DisableStart = false;
            DisableNext = true;
            await base.OnInitializedAsync();
        }

        protected void RadioChange(ChangeEventArgs args)
        {
            RadioIdValue = args.Value.ToString();
        }

        private void StartTimer()
        {
            aTimer?.Stop();
            Counter = _counter;
            aTimer = new System.Timers.Timer(1000);
            aTimer.Elapsed += CountDownTimer;
            aTimer.Enabled = true;
        }

        private void CountDownTimer(object source, System.Timers.ElapsedEventArgs e)
        {
            if (Counter > 0)
            {
                Counter -= 1;
            }
            else
            {
                aTimer.Enabled = false;
            }
            InvokeAsync(StateHasChanged);
        }


        protected async Task Start()
        {
            IsShowResult = false;
            DisableStart = true;
            DisableNext = false;

            var createExamModel = new CreateExamModel() {StudentId = Session.GetCurrentUser().Id};
            currentExamId = await ExamService.CreateAsync(createExamModel);

            Exam = await ExamService.GetByIdAsync(currentExamId);
            QuestionsCount = Exam.QuestionsCount.ToString();

            await GetNextExam();
        }

        protected async Task Next()
        {
            int.TryParse(RadioIdValue, out var answerId);
            var updateExamDetailRequest = new UpdateExamDetailModel()
            {
                AnswerId = Counter == 0 ? Counter : answerId,
                Id = GetNextResponse.Id,
                ExamId = currentExamId,
                IsFinished = true,
                QuestionId = GetNextResponse.QuestionId
            };
            await ExamDetailsService.UpdateAsync(updateExamDetailRequest);

            if (RemainsCount > 0)
            {
                await GetNextExam();
            }
            else
            {
                await FinishExam();
            }

            RadioIdValue = null;
        }

        private async Task GetNextExam()
        {
            GetNextResponse = await ExamDetailsService.GetNextByExamIdAsync(currentExamId);
            QuestionNumber = GetNextResponse.SerialNumber.ToString();
            RemainsCount = Exam.QuestionsCount - GetNextResponse.SerialNumber;

            await InvokeAsync(StartTimer);
        }

        private async Task FinishExam()
        {
            await ExamService.FinishAsync(currentExamId);
            Exam = await ExamService.GetByIdAsync(currentExamId);

            GetNextResponse = null;
            IsShowResult = true;
            DisableStart = false;
            DisableNext = true;
        }
    }
}
