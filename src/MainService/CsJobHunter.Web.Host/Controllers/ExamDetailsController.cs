﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Exams;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;

namespace CsJobHunter.Web.Host.Controllers
{
    /// <summary>
    /// Exam Details Controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class ExamDetailsController : ControllerBase
    {
        private readonly ILogger<ExamDetailsController> _logger;
        private readonly IExamDetailsService _examDetailsService;

        public ExamDetailsController(
            ILogger<ExamDetailsController> logger,
            IExamDetailsService examDetailsService)
        {
            _logger = logger;
            _examDetailsService = examDetailsService;
        }

        /// <summary>
        /// Provides all exam details by exam id
        /// </summary>
        /// <param name="examId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<ExamWithDetailResponse>> GetAllByExamIdAsync([FromQuery] long examId)
        {
            var examDetails = await _examDetailsService.GetAllByExamIdAsync(examId);

            return StatusCode(StatusCodes.Status200OK, examDetails);
        }

        /// <summary>
        /// Provides exam detail by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ExamWithDetailResponse>> GetByIdAsync([FromRoute]long id)
        {
            var examDetails = await _examDetailsService.GetByIdAsync(id);

            return StatusCode(StatusCodes.Status200OK, examDetails);
        }

        /// <summary>
        /// Creates next available exam detail by exam id
        /// </summary>
        /// <param name="idExam"></param>
        /// <returns></returns>
        [HttpGet("Next/{idExam}")]
        public async Task<ActionResult<GetNextResponse>> GetNextAsync([FromRoute]long idExam)
        {
            var examDetails = await _examDetailsService.GetNextByExamIdAsync(idExam);

            return StatusCode(StatusCodes.Status200OK, examDetails);
        }

        /// <summary>
        /// Updates exam detail
        /// </summary>
        /// <param name="createExamDetailsRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ExamWithDetailResponse>> CreateAsync
            ([FromBody] CreateExamDetailsRequest createExamDetailsRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            bool res = await _examDetailsService.CreateAsync(createExamDetailsRequest);

            if (res)
                return StatusCode(StatusCodes.Status200OK, res);

            return StatusCode(StatusCodes.Status400BadRequest);
        }

        /// <summary>
        /// Updates exam detail
        /// </summary>
        /// <param name="updateExamDetailsRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<ExamWithDetailResponse>> UpdateAsync
            ([FromBody] UpdateExamDetailsRequest updateExamDetailsRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var res = await _examDetailsService.UpdateAsync(updateExamDetailsRequest);

            if (res)
                return StatusCode(StatusCodes.Status200OK, res);

            return StatusCode(StatusCodes.Status400BadRequest);
        }
    }
}
