﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Questions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CsJobHunter.Web.Host.Controllers
{
    /// <summary>
    /// Questions Controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class QuestionsController : ControllerBase
    {
        private readonly ILogger<QuestionsController> _logger;
        private readonly IQuestionService _questionService;

        public QuestionsController(
            ILogger<QuestionsController> logger,
            IQuestionService questionService)
        {
            _logger = logger;
            _questionService = questionService;
        }


        /// <summary>
        /// Get Question by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<QuestionResponse>> GetByIdAsync(long id)
        {
            var questionResponse = await _questionService.GetByIdAsync(id);

            return Ok(questionResponse);
        }

        /// <summary>
        /// Creates Question
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<long>> CreateAsync([FromBody] QuestionAddUpdateRequest request)
        {
            var id = await _questionService.CreateAsync(request);

            return Ok(id);
        }

        /// <summary>
        /// Updates Question
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateAsync(long id, [FromBody] QuestionAddUpdateRequest request)
        {
            await _questionService.UpdateAsync(id, request);

            return Ok();
        }

        /// <summary>
        /// Deletes Question by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteAsync(long id)
        {
            await _questionService.DeleteAsync(id);

            return Ok();
        }
    }
}
