﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Students;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CsJobHunter.Web.Host.Controllers
{
    /// <summary>
    /// Students Controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class StudentsController : ControllerBase
    {
        private readonly ILogger<StudentsController> _logger;
        private readonly IStudentService _studentService;

        public StudentsController(ILogger<StudentsController> logger,
            IStudentService studentService)
        {
            _logger = logger;
            _studentService = studentService;
        }

        /// <summary>
        /// Provides all Students
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<StudentResponse>> GetAllAsync()
        {
            var studentResponse = await _studentService.GetAllAsync();

            return Ok(studentResponse);
        }

        /// <summary>
        /// Provides Student by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<StudentResponse>> GetByIdAsync(long id)
        {
            var studentResponse = await _studentService.GetByIdAsync(id);

            return Ok(studentResponse);
        }

        /// <summary>
        /// Register Student
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<long>> RegisterAsync([FromBody] StudentRegisterRequest request)
        {
            var id = await _studentService.RegisterAsync(request);

            return Ok(id);
        }

        /// <summary>
        /// Update Student
        /// </summary>
        [HttpPut]
        public async Task<ActionResult> UpdateAsync([FromBody] StudentUpdateRequest request)
        {
            await _studentService.UpdateAsync(request);

            return Ok();
        }

        /// <summary>
        /// Change Student Password
        /// </summary>
        [HttpPut("ChangePassword")]
        public async Task<ActionResult> ChangePasswordAsync([FromBody] StudentChangePasswordRequest request)
        {
            await _studentService.ChangePasswordAsync(request);

            return Ok();
        }

        /// <summary>
        /// Deletes Student
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteAsync(long id)
        {
            await _studentService.DeleteAsync(id);

            return Ok();
        }
    }
}
