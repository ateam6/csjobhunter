﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Exams;
using CsJobHunter.Web.Host.RabbitMQ;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsJobHunter.Web.Host.Controllers
{
    /// <summary>
    /// Exam controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class ExamsController : ControllerBase
    {
        private readonly ILogger<ExamsController> _logger;
        private readonly IExamService _examService;
        private readonly IEmailSenderByAddresses _emailSenderByAddresses;

        public ExamsController(
            ILogger<ExamsController> logger,
            IExamService examService,
            IEmailSenderByAddresses emailSenderByAddresses)
        {
            _logger = logger;
            _examService = examService;
            _emailSenderByAddresses = emailSenderByAddresses;
        }

        /// <summary>
        /// Provides all exams
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ExamResponse>>> GetAllAsync()
        {
            var exams = await _examService.GetAllAsync();

            return StatusCode(StatusCodes.Status200OK, exams);
        }

        /// <summary>
        /// Provides all exams
        /// </summary>
        /// <returns></returns>
        [HttpGet("students/{studentId}")]
        public async Task<ActionResult<IEnumerable<ExamResponse>>> GetAllByStudentIdAsync([FromRoute] long studentId)
        {
            var exams = await _examService.GetAllByStudentIdAsync(studentId);

            return StatusCode(StatusCodes.Status200OK, exams);
        }

        /// <summary>
        /// Provides exam by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ExamResponse>> GetByIdAsync([FromRoute] int id)
        {
            var exam = await _examService.GetByIdAsync(id);

            return StatusCode(StatusCodes.Status200OK, exam);
        }


        /// <summary>
        /// Creates exam by id
        /// </summary>
        /// <param name="createExamRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<long>> CreateAsync([FromBody] CreateExamRequest createExamRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var examId = await _examService.CreateAsync(createExamRequest);

            return StatusCode(StatusCodes.Status201Created, examId);
        }

        /// <summary>
        /// Updates exam
        /// </summary>
        /// <param name="updateExamRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateExamRequest updateExamRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _examService.UpdateAsync(updateExamRequest);

            var emailDto = await _examService.GetEmailAsync(updateExamRequest);

            _emailSenderByAddresses.SendToTheFollowingAddresses(emailDto);

            return StatusCode(StatusCodes.Status200OK);
        }
    }
}
