﻿using CsJobHunter.Application.Dtos;
using CsJobHunter.Core.Models.ExamAggregate;
using CsJobHunter.EmailSender.Options;
using Microsoft.Extensions.Options;

namespace CsJobHunter.Web.Host.RabbitMQ
{
    public class RabbitSenderByAddress : IEmailSenderByAddresses
    {
        private readonly Client _rabbitClient;
        private readonly string _queue;

        public RabbitSenderByAddress(IOptions<RabbitMqOptions> rabbitMqOptions)
        {
            _queue = rabbitMqOptions.Value.Queue;

            _rabbitClient = new Client(
                rabbitMqOptions.Value.HostName,
                rabbitMqOptions.Value.UserName,
                rabbitMqOptions.Value.Password);

        }

        public void SendToTheFollowingAddresses(EmailDto emailDto)
        {
            var jsonString = Json.CreateJson(emailDto);
            _rabbitClient.Send(jsonString, _queue);
        }
    }
}
