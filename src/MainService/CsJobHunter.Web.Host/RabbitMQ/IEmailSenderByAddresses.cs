﻿using CsJobHunter.Application.Dtos;

namespace CsJobHunter.Web.Host.RabbitMQ
{
    public interface IEmailSenderByAddresses
    {
        void SendToTheFollowingAddresses(EmailDto emailDto);
    }
}
