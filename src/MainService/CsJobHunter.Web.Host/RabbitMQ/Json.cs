﻿using System.Text.Json;
using CsJobHunter.Application.Dtos;

namespace CsJobHunter.Web.Host.RabbitMQ
{
    public static class Json
    {
        public static string CreateJson(EmailDto emailDto)
        {
            return JsonSerializer.Serialize<EmailDto>(emailDto);
        }
    }
}