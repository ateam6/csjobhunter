using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Services;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.DataAccess;
using CsJobHunter.DataAccess.Repositories;
using CsJobHunter.EmailSender.Options;
using CsJobHunter.Web.Host.Extensions;
using CsJobHunter.Web.Host.RabbitMQ;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CsJobHunter.Web.Host
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            IdentityHostUrl = configuration.GetValue<string>("IdentityHostUrl");
            Scope = configuration.GetValue<string>("Scope");
        }

        private string IdentityHostUrl { get; }
        private string Scope { get; }
        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CsJobHunterDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("Default")).EnableSensitiveDataLogging());

            services.AddDatabaseDeveloperPageExceptionFilter();

            services
                .AddTransient(typeof(IRepository<>), typeof(BaseRepository<>))
                .AddTransient<IStudentRepository, StudentRepository>()
                .AddTransient<IStudentService, StudentService>()
                .AddTransient<IExamService, ExamService>()
                .AddTransient<IExamDetailsRepository, ExamDetailsRepository>()
                .AddTransient<IExamRepository, ExamRepository>()
                .AddTransient<IExamDetailsService, ExamDetailsService>()
                .AddTransient<IQuestionRepository, QuestionRepository>()
                .AddTransient<IQuestionService, QuestionService>()
                .AddTransient<IEmailSenderByAddresses, RabbitSenderByAddress>();

            services.Configure<RabbitMqOptions>(Configuration.GetSection(
                RabbitMqOptions.RabbitMq));

            services.AddControllers();

            services.AddRazorPages();

            services.AddHealthChecks();

            services.AddWebApiAuthentication(IdentityHostUrl)
                .AddWebApiAuthorization(Scope);

            services.AddSwaggerWithAuth(IdentityHostUrl, Scope);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "CsJobHunter Web Api v1");
                    options.OAuthClientId("webapi.swaggerui");
                    options.OAuthAppName("WebApi Swagger UI");
                    options.OAuthScopes(Scope);
                });
                app.MigrateAndSeedDb(development: true);
            }

            app.UseRouting();

            app.UseCors(cors => cors
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true)
                .AllowCredentials()
            );

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseHealthChecks("/health");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.MapRazorPages();
            });
        }
    }
}
