﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Questions;
using CsJobHunter.Application.Services;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.QuestionAggregate;
using CsJobHunter.DataAccess;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CsJobHunter.UnitTests.Application
{
    public class QuestionServiceTests
    {
        private readonly Mock<IQuestionRepository> QuestionRepository = new Mock<IQuestionRepository>();
        private readonly IQuestionService QuestionService;

        public QuestionServiceTests()
        {
            var cancellationToken = new CancellationToken(false);
            QuestionRepository.Setup(c => c.SaveChangesAsync(cancellationToken)).Returns(Task.FromResult(1));
            QuestionService = new QuestionService(QuestionRepository.Object);
        }

        private IEnumerable<Question> CreateQuestions()
        {
            IEnumerable<Question> testQuestions = new[]
            {
                new TestQuestionBuilder().WithAnswers().Build(),
                new TestQuestionBuilder().WithAnswers().Build(),
                new TestQuestionBuilder().WithAnswers().Build(),
            };

            return testQuestions;
        }

        [Fact]
        public async Task GetByIdAsync_Returns_QuestionResponse()
        {
            // Arrange
            var question = CreateQuestions().First();
            QuestionRepository.Setup(e => e.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(question));

            // Act
            var result = await QuestionService.GetByIdAsync(1);

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<QuestionResponse>(result);
        }

        [Fact]
        public async Task UpdateAsync_Changes_Question()
        {
            // Arrange
            var question = CreateQuestions().First();
            QuestionRepository.Setup(e => e.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(question));

            var questionAddUpdateRequest = new QuestionAddUpdateRequest()
            {
                QuestionText = "Updated text",
                Code = "Updated code",
                Answers = new List<AnswerDto>()
                {
                    new AnswerDto() {Text = "test1", IsRight = false},
                    new AnswerDto() {Text = "test1", IsRight = true},
                    new AnswerDto() {Text = "test1", IsRight = false},
                }
            };

            // Act
            async Task Func() => await QuestionService.UpdateAsync(1, questionAddUpdateRequest);

            // Assert
            Assert.Equal(Task.CompletedTask.Status, Func().Status);
            Assert.Equal(question.Text.Value, questionAddUpdateRequest.QuestionText);
            Assert.Equal(question.Code.Value, questionAddUpdateRequest.Code);
        }

        [Fact]
        public async Task DeleteAsync_Succeedes()
        {
            // Arrange
            var question = CreateQuestions().First();
            QuestionRepository.Setup(e => e.RemoveAsync(It.IsAny<long>()))
                .Returns(Task.CompletedTask);
            // Act
            async Task Func() => await QuestionService.DeleteAsync(1);

            // Assert
            Assert.Equal(Task.CompletedTask.Status, Func().Status);
        }
    }
}
