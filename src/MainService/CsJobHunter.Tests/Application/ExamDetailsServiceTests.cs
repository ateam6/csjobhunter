﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Exams;
using CsJobHunter.Application.Services;
using CsJobHunter.Core.Exceptions;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.ExamAggregate;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CsJobHunter.Core.Models.QuestionAggregate;
using Xunit;

namespace CsJobHunter.UnitTests.Application
{
    public class ExamDetailsServiceTests
    {
        private readonly Mock<IExamDetailsRepository> ExamDetailsRepository = new();
        private readonly Mock<IRepository<Exam>> ExamRepository = new();
        private readonly Mock<IQuestionRepository> QuestionRepository = new();
        private readonly Mock<IRepository<Answer>> AnswerRepository = new();
        private readonly IExamDetailsService ExamDetailsService;

        public ExamDetailsServiceTests()
        {
            var cancellationToken = new CancellationToken(false);
            ExamDetailsService = new ExamDetailsService(
                ExamDetailsRepository.Object, 
                ExamRepository.Object,
                AnswerRepository.Object,
                QuestionRepository.Object);
        }

        private IEnumerable<ExamDetail> CreateExamDetails()
        {
            var question1 = new TestQuestionBuilder().WithAnswers().Build();
            var question2 = new TestQuestionBuilder().WithAnswers().Build();

            IEnumerable<ExamDetail> testExamDetails = new[]
            {
                new ExamDetail()
                {
                    ExamId = 1,
                    Exam = new Exam(),
                    QuestionId = 1,
                    Question = question1,
                    Answer =   question1.Answers.First()
                },
                new ExamDetail()
                {
                ExamId = 2,
                Exam = new Exam(),
                QuestionId = 2,
                Question = question2,
                Answer =   question2.Answers.First()
                }
            };

            return testExamDetails;
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async Task GetAllAsync_Returns_All_ExamDetails(int id)
        {
            // Arrange
            var examDetails = CreateExamDetails();
            ExamDetailsRepository.Setup(e => e.GetAllByExamIdAsync(id))
                .Returns(Task.FromResult(examDetails.Where(e => e.ExamId == id)));

            // Act
            var result = await ExamDetailsService.GetAllByExamIdAsync(id);

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<ExamWithDetailResponse>>(result);
        }

        [Fact]
        public async Task GetByIdAsync_Returns_StudentResponse()
        {
            // Arrange
            var examDetail = CreateExamDetails().First();
            ExamDetailsRepository.Setup(e => e.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(examDetail));

            // Act
            var result = await ExamDetailsService.GetByIdAsync(1);

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<ExamWithDetailResponse>(result);
        }

        [Fact]
        public async Task GetByIdAsync_ThrowsException_Given_Id_NotExists()
        {
            // Arrange
            var examDetails = CreateExamDetails();
            var id = examDetails.Select(e => e.Id).Max() + 1;

            ExamDetailsRepository.Setup(e => e.GetAllByExamIdAsync(id))
                .Returns(Task.FromResult(examDetails.Where(e => e.ExamId == id)));

            //var action = await ExamDetailsService.GetByIdAsync(1);
            async Task Func() => await ExamDetailsService.GetByIdAsync(id);

            await Assert.ThrowsAsync<EntityNotFoundException>(Func);
        }
    }
}
