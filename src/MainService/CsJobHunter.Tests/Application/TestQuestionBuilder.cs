﻿using CsJobHunter.Core.Models.QuestionAggregate;
using System.Collections.Generic;

namespace CsJobHunter.UnitTests.Application
{
    public class TestQuestionBuilder
    {
        private readonly Question _instance;

        public TestQuestionBuilder()
            : this("Simple Test Question")
        {
        }

        public TestQuestionBuilder(string questionText)
        {
            _instance = Question.Create(questionText, null, new List<AnswerDescription>{
                new AnswerDescription() { Text = "test1", IsRight = false },                
                new AnswerDescription() { Text = "test3", IsRight = true }});
        }

        public TestQuestionBuilder WithAnswers()
        {
            _instance.SetAnswers(new List<AnswerDescription>
            {
                new AnswerDescription() { Text = "test1", IsRight = false },
                new AnswerDescription() { Text = "test2", IsRight = false },
                new AnswerDescription() { Text = "test3", IsRight = true }
            });
            return this;
        }

        public Question Build()
        {
            return _instance;
        }
    }
}
