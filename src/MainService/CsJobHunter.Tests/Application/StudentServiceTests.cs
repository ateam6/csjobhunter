﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Students;
using CsJobHunter.Application.Services;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.StudentAggregate;
using CsJobHunter.DataAccess;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CsJobHunter.UnitTests.Application
{
    public class StudentServiceTests
    {
        private readonly Mock<IStudentRepository> StudentRepository = new Mock<IStudentRepository>();
        private readonly IStudentService StudentService;

        public StudentServiceTests()
        {
            var cancellationToken = new CancellationToken(false);
            StudentRepository.Setup(c => c.SaveChangesAsync(cancellationToken)).Returns(Task.FromResult(1));
            StudentService = new StudentService(StudentRepository.Object);
        }

        private IEnumerable<Student> CreateStudents()
        {
            IEnumerable<Student> testStudents = new[]
            {
                new Student(Login.Create("loginofstudent1").Value,
                    Name.Create("FirstName1", "LastName1").Value,
                    Email.Create("testemail1@test.com").Value,
                    Password.Create("PassWord123$").Value),
                new Student(Login.Create("loginofstudent2").Value,
                    Name.Create("FirstName2", "LastName2").Value,
                    Email.Create("testemail2@test.com").Value,
                    Password.Create("PassWord123$").Value),
            };

            return testStudents;
        }

        [Fact]
        public async Task GetAllAsync_Returns_All_StudentResponses()
        {
            // Arrange
            var students = CreateStudents();
            StudentRepository.Setup(e => e.GetAllAsync())
                .Returns(Task.FromResult(students));

            // Act
            var result = await StudentService.GetAllAsync();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<StudentResponse>>(result);
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public async Task GetByIdAsync_Returns_StudentResponse()
        {
            // Arrange
            var students = CreateStudents();
            StudentRepository.Setup(e => e.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(students.First()));

            // Act
            var result = await StudentService.GetByIdAsync(1);

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<StudentResponse>(result);
        }

        [Fact]
        public async Task UpdateAsync_Changes_Student()
        {
            // Arrange
            var student = CreateStudents().First();
            StudentRepository.Setup(e => e.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(student));
            var studentUpdateRequest = new StudentUpdateRequest()
            {
                Id = student.Id,
                FirstName = "UpdatedName",
                LastName = "UpdatedLastName",
                Email = "updatedemail@test.com"
            };

            // Act
            async Task Func() => await StudentService.UpdateAsync(studentUpdateRequest);

            // Assert
            Assert.Equal(Task.CompletedTask.Status, Func().Status);
            Assert.Equal(student.Email.Value, studentUpdateRequest.Email);
            Assert.Equal(student.Name.First, studentUpdateRequest.FirstName);
            Assert.Equal(student.Name.Last, studentUpdateRequest.LastName);
        }

        [Fact]
        public async Task DeleteAsync_Succeedes()
        {
            // Arrange
            var student = CreateStudents().First();
            StudentRepository.Setup(e => e.RemoveAsync(It.IsAny<long>()))
                .Returns(Task.CompletedTask);
            // Act
            async Task Func() => await StudentService.DeleteAsync(1);

            // Assert
            Assert.Equal(Task.CompletedTask.Status, Func().Status);
        }
    }
}
