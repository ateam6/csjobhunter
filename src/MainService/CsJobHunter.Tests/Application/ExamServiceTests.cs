﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Exams;
using CsJobHunter.Application.Services;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.Constants.ExamAggregate;
using CsJobHunter.Core.Models.ExamAggregate;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CsJobHunter.Web.Host.RabbitMQ;
using Xunit;

namespace CsJobHunter.UnitTests.Application
{
    public class ExamServiceTests
    {
        private readonly Mock<IExamRepository> ExamRepository = new();
        private readonly Mock<IQuestionRepository> QuestionRepository = new();
        private readonly Mock<IExamDetailsRepository> ExamDetailsRepository = new();
        private readonly Mock<IEmailSenderByAddresses> EmailSenderByAddresses = new();
        private readonly Mock<IStudentRepository> StudentRepository = new();
        private readonly IExamService ExamService;

        public ExamServiceTests()
        {
            var cancellationToken = new CancellationToken(false);
            ExamRepository.Setup(c => c.SaveChangesAsync(cancellationToken)).Returns(Task.FromResult(1));
            ExamService = new ExamService(
                ExamRepository.Object,
                ExamDetailsRepository.Object,
                QuestionRepository.Object,
                StudentRepository.Object);
        }

        private IEnumerable<Exam> CreateExams()
        {
            var testExams = new List<Exam>()
            {
                new Exam()
                {
                    DateTimeBegin = new DateTime(2001, 01, 01),
                    Mark = Mark.Fail,
                    IsPassed = false,
                    Status = Status.Created,
                    StudentId = 1,
                    ExamDetails = new List<ExamDetail>()
                },
                new Exam()
                {
                    DateTimeBegin = new DateTime(2002, 02, 02),
                    Mark = Mark.Excellent,
                    IsPassed = true,
                    Status = Status.Created,
                    StudentId = 2,
                    ExamDetails = new List<ExamDetail>()
                }
            };

            return testExams;
        }

        [Fact]
        public async Task GetAllAsync_Returns_All_ExamResponses()
        {
            // Arrange
            var exams = CreateExams();
            ExamRepository.Setup(e => e.GetAllAsync())
                .Returns(Task.FromResult(exams));

            // Act
            var result = await ExamService.GetAllAsync();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<ExamResponse>>(result);
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public async Task GetByIdAsync_Returns_ExamResponse()
        {
            // Arrange
            var exam = CreateExams().First();
            ExamRepository.Setup(e => e.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(exam));

            // Act
            var result = await ExamService.GetByIdAsync(1);

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<ExamResponse>(result);
        }

        [Fact]
        public async Task UpdateAsync_Changes_Exam()
        {
            // Arrange
            var exam = CreateExams().First();
            ExamRepository.Setup(e => e.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(exam));

            var updateExamRequest = new UpdateExamRequest()
            {
                Id = 1,
                IsFinished = true
            };

            // Act
            async Task Func() => await ExamService.UpdateAsync(updateExamRequest);

            // Assert
            Assert.Equal(Task.CompletedTask.Status, Func().Status);
            Assert.Equal(exam.IsFinished, updateExamRequest.IsFinished);
        }
    }
}
