﻿using CsJobHunter.Core.Models.QuestionAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CsJobHunter.UnitTests.Core.QuestionsWithAnswers
{

    public class Question_Create
    {
        [Theory]
        [MemberData(nameof(GetValidTestCases))]
        public void QuestionCreate_ValidData_Success(string questionText, string codeText, (string answerText, bool isRight)[] sourceAnswers)
        {
            // Arrange
            var answers = sourceAnswers
                            .Select(x => new AnswerDescription
                            {
                                Text = x.answerText,
                                IsRight = x.isRight
                            }).ToList();

            // Act
            var question = Question.Create(questionText, codeText, answers);

            // Assert
            Assert.Equal(questionText, question.Text.Value);
            Assert.Equal(codeText, question.Code?.Value);
            Assert.Equal(answers.Count, question.Answers.Count);

            // Сравниваем значения sourceAnswers и question.Answers (решение в лоб).
            for (int i = 0; i < sourceAnswers.Length; i++)
            {
                var answerInQuestion = question.Answers.ElementAt(i);
                Assert.Equal(sourceAnswers[i].answerText, answerInQuestion.Text.Value);
                Assert.Equal(sourceAnswers[i].isRight, answerInQuestion.IsRight);
            }
        }
        public static IEnumerable<object[]> GetValidTestCases()
        {
            yield
                return new object[] {
                    "Что такое int?",
                    null,
                    new (string,bool)[] {
                        ("Тип данных",true),
                        ("Класс", false),
                        ("Интерфейс", false)
                    }
                };

            yield
                return new object[] {
                    "Чему будет равно значение переменной str после выполнения операторов?",
                    "string str = \"Hello world\";\nstring s = str.Remove(5, 6)",
                    new (string, bool)[]
                    {
                        ("\"Hello\"", false),
                        ("\"Hello world\"", true),
                        ("Код не скомпилируется", false)
                    }
                };
        }

        [Theory]
        [MemberData(nameof(GetInValidTestCases))]
        public void QuestionCreate_InValidData_Exception(string questionText, string codeText, (string answerText, bool isRight)[] sourceAnswers)
        {
            // Arrange
            List<AnswerDescription> answers = new();

            if (sourceAnswers != null)
            {
                answers = sourceAnswers
                                .Select(x => new AnswerDescription
                                {
                                    Text = x.answerText,
                                    IsRight = x.isRight
                                }).ToList();
            }
                        
            Action act = () => Question.Create(questionText, codeText, answers);

            //  Act, Assert
            Assert.ThrowsAny<Exception>(act);
        }

        public static IEnumerable<object[]> GetInValidTestCases()
        {
            yield
                return new object[] {
                    null,
                    "Any code",
                    new (string,bool)[] {
                        ("Тип данных",true),
                        ("Класс", false),
                        ("Интерфейс", false)
                    }
                };

            yield
                return new object[] {
                    "Что такое int?",
                    null,
                    new (string,bool)[] {
                        ("Класс", false),
                        ("Интерфейс", false)
                    }
                };

            yield
                return new object[] {
                    "Что такое int?",
                    null,
                    new (string,bool)[] {
                        ("Тип данных",true)
                    }
                };

            yield
                return new object[] {
                    "",
                    null,
                    new (string,bool)[] {
                        ("Тип данных",true),
                        ("Класс", false),
                        ("Интерфейс", false)
                    }
                };

            yield
                return new object[] {
                    "Что такое int?",
                    "",
                    new (string,bool)[] {
                        ("Тип данных",true),
                        ("Класс", false),
                        ("Интерфейс", false)
                    }
                };

            yield
                return new object[] {
                    "Что такое int?",
                    null,
                    new (string,bool)[] {
                        ("    ",true),
                        ("Класс", false),
                        ("Интерфейс", false)
                    }
                };

            yield
                return new object[] {
                    "Что такое int?",
                    null,
                    null
                };

        }
    }
}
