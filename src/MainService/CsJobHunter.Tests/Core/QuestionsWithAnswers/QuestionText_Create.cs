﻿using CSharpFunctionalExtensions;
using CsJobHunter.Core.Models.QuestionAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CsJobHunter.UnitTests.Core.QuestionsWithAnswers
{
    public class QuestionText_Create
    {
        [Theory]
        [InlineData("Что такое int?")]
        [InlineData("Возможно ли в операторах LINQ использовать одновременно декларативный синтаксис и синтаксис методов?")]

        public void QuestionTextCreate_ValidText_Success(string text)
        {
            // Arrange
            // Act
            var questionText = QuestionText.Create(text);

            // Assert
            Assert.False(questionText.IsFailure);
            Assert.Equal(text, questionText.Value.Value);
        }

        [Theory]
        [InlineData("")]
        [InlineData("                      ")]
        [InlineData("> 256 символов -------------- Возможно ли в операторах LINQ использовать одновременно декларативный синтаксис и синтаксис методов?---------------------------------------------------------------------------------------------------------------------------------------")]

        public void QuestionTextCreate_InValidText_Exception(string text)
        {
            // Arrange
            // Act
            var questionText = QuestionText.Create(text);

            // Assert
            Assert.True(questionText.IsFailure);            
            var ex = Assert.Throws<ResultFailureException>(() => questionText.Value);
        }
    }
}
