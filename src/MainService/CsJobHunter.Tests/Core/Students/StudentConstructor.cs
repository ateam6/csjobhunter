using CsJobHunter.Core.Models.StudentAggregate;
using Xunit;

namespace CsJobHunter.UnitTests.Core.Students

{
    public class StudentConstructor
    {
        private Login login = Login.Create("jsmith").Value;
        private Name name = Name.Create("John", "Smith").Value;
        private Email email = Email.Create("test@test.com").Value;
        private Password password = Password.Create("Pa$$word91").Value;

        private Student _student = null;

        private Student CreateStudent()
        {
            return new Student(login, name, email, password);
        }

        [Fact]
        public void Initialize_student_params_equals_properties()
        {
            _student = CreateStudent();

            Assert.Equal(login, _student.Login);
            Assert.Equal(name, _student.Name);
            Assert.Equal(email, _student.Email);
            Assert.Equal(password, _student.Password);
        }
    }
}
