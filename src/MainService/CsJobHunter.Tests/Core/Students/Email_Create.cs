﻿using CSharpFunctionalExtensions;
using CsJobHunter.Core.Models.StudentAggregate;
using Xunit;

namespace CsJobHunter.UnitTests.Core.Students
{
    public class Email_Create
    {
        [Theory]
        [InlineData("abc@gmail.com")]
        [InlineData("aba_a1203.03@mail.ru")]
        public void Create_correct_email_creates_successful_result_with_value(string email)
        {
            var emailResult = Email.Create(email);

            Assert.False(emailResult.IsFailure);
            Assert.Equal(email, emailResult.Value.Value);
        }

        [Theory]
        [InlineData("abcgmail.com")]
        [InlineData("aba a1203.03@mail.ru")]
        [InlineData("aba a1203.03@mail.rudfsdfsd")]
        public void Create_incorrect_email_creates_failed_result_without_value(string email)
        {
            var emailResult = Email.Create(email);

            Assert.True(emailResult.IsFailure);

            var ex = Assert.Throws<ResultFailureException>(() => emailResult.Value);

            Assert.Equal("Email address is not valid", ex.Error);
        }
    }
}
