﻿using CsJobHunter.Core.Models.ExamAggregate;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsJobHunter.Core.Interfaces
{
    public interface IExamDetailsRepository : IRepository<ExamDetail>
    {
        Task<IEnumerable<ExamDetail>> GetAllByExamIdAsync(long id);
        Task<ExamDetail> GetNextByExamIdAsync(long examId);
        Task<int> GetCountRemainIdAsync(long id);
    }
}
