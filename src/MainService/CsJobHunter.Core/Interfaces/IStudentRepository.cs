﻿using System.Collections.Generic;
using CsJobHunter.Core.Models.StudentAggregate;
using System.Threading.Tasks;

namespace CsJobHunter.Core.Interfaces
{
    public interface IStudentRepository : IRepository<Student>
    {
        Task<IEnumerable<Student>> GetAllAsync();
        Task<Student> GetByLoginAsync(string valueLogin);
        Task RemoveAsync(long id);
    }
}
