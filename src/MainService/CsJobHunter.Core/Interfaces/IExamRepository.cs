﻿using System.Collections.Generic;
using CsJobHunter.Core.Models.ExamAggregate;
using System.Threading.Tasks;

namespace CsJobHunter.Core.Interfaces
{
    public interface IExamRepository : IRepository<Exam>
    {
        Task<Exam> GetByIdAsync(long id);
        Task<IEnumerable<Exam>> GetAllByStudentIdAsync(long id);
    }
}
