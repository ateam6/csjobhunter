﻿using CsJobHunter.Core.Models.QuestionAggregate;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsJobHunter.Core.Interfaces
{
    public interface IQuestionRepository : IRepository<Question>
    {
        Task<IEnumerable<Question>> GetRandomSetOfQuestionsAsync(byte questionsNumInSet);
    }
}
