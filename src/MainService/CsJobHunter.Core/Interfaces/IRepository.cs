﻿using CsJobHunter.Core.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CsJobHunter.Core.Interfaces
{
    public interface IRepository<TEntity>
        where TEntity : Entity
    {
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<TEntity> GetByIdAsync(long id);
        Task InsertAsync(TEntity entity);
        Task RemoveAsync(long id);
        Task SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
