﻿using System;

namespace CsJobHunter.Core.Exceptions
{
    [Serializable]
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException()
        {
        }

        public EntityNotFoundException(Type entityType, long id)
            : base($"Entity not found. Type: {entityType}, id: {id}")
        {
        }

        public EntityNotFoundException(Type entityType)
            : base($"Entity not found. Type: {entityType}")
        {
        }
    }
}
