﻿using CSharpFunctionalExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CsJobHunter.Core.Models.StudentAggregate
{
    public class Password : ValueObject
    {
        private const int MinLength = 8;
        private const int MaxLength = 20;

        [Required]
        public string Value { get; }

        private Password()
        {
        }

        private Password(string password)
            : this()
        {
            Value = password;
        }

        private static string Encode(string password)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(password));
        }

        public static Result<Password> Create(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                return Result.Failure<Password>("Password should not be empty");

            password = password.Trim();

            if (password.Length < MinLength)
                return Result.Failure<Password>("Password is too short (should be more than 7 symbols length)");
            if (password.Length > MaxLength)
                return Result.Failure<Password>("Password is too long");

            password = Password.Encode(password);

            return Result.Success(new Password(password));
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
