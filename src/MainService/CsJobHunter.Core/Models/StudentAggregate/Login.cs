﻿using CSharpFunctionalExtensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CsJobHunter.Core.Models.StudentAggregate
{
    public class Login : ValueObject
    {
        private const int MinLength = 4;
        private const int MaxLength = 20;

        [Required]
        public string Value { get; }

        private Login()
        {
        }

        private Login(string login)
            : this()
        {
            Value = login;
        }

        public static Result<Login> Create(string login)
        {
            if (string.IsNullOrWhiteSpace(login))
                return Result.Failure<Login>("Login should not be empty");

            login = login.Trim();

            if (login.Length < MinLength)
                return Result.Failure<Login>("Login is too short");
            if (login.Length > MaxLength)
                return Result.Failure<Login>("Login is too long");

            var regex = new Regex(@"^[aA-zZ,0-9]+$");
            if (!regex.IsMatch(login))
                return Result.Failure<Login>("Login is not in correct format (use only letters and numbers.");

            return Result.Success(new Login(login));
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
