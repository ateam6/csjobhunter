﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CsJobHunter.Core.Models.ExamAggregate;

namespace CsJobHunter.Core.Models.StudentAggregate
{
    public class Student : Entity
    {
        [Required]
        public virtual Login Login { get; private set; }

        [Required]
        public virtual Name Name { get; private set; }

        [Required]
        public virtual Email Email { get; private set; }

        [Required]
        public virtual Password Password { get; private set; }

        public virtual ICollection<Exam> Exams { get; private set; }

        protected Student()
        {
        }

        public Student(Login login, Name name, Email email, Password password)
            : this()
        {
            Login = login;
            Name = name;
            Email = email;
            Password = password;
        }

        public void Update(Name name, Email email)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Email = email ?? throw new ArgumentNullException(nameof(email));
        }

        public void ChangePassword(Password password)
        {
            Password = password ?? throw new ArgumentNullException(nameof(password));
        }
    }
}