﻿using CSharpFunctionalExtensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CsJobHunter.Core.Models.StudentAggregate
{
    public class Email : ValueObject
    {
        [Required]
        public string Value { get; }

        protected Email()
        {
        }

        private Email(string value)
            : this()
        {
            Value = value;
        }

        private static bool IsValid(string value)
        {
            try
            {
                var email = new System.Net.Mail.MailAddress(value);
                return value == email.Address;
            }
            catch
            {
                return false;
            }
        }

        public static Result<Email> Create(string value)
        {
            if (!Email.IsValid(value))
                return Result.Failure<Email>("Email address is not valid");

            return Result.Success(new Email(value));
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
