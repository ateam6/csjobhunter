﻿namespace CsJobHunter.Core.Models
{
    public abstract class Entity
    {
        public long Id { get; private set; }

        protected Entity()
        {
        }

        protected Entity(long id)
            : this()
        {
            Id = id;
        }
    }
}
