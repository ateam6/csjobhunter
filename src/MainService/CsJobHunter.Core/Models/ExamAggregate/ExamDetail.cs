﻿using CsJobHunter.Core.Models.QuestionAggregate;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CsJobHunter.Core.Models.ExamAggregate
{
    public class ExamDetail : Entity
    {
        [Required]
        public long ExamId { get; set; }
        [Required]
        public Exam Exam { get; set; }

        public long SerialNumber { get; set; }
        public bool IsFinished { get; set; }
        public bool IsCorrectlyAnswered { get; set; }

        public long QuestionId { get; set; }

        [ForeignKey("QuestionId")]
        public Question Question { get; set; }

        public long? AnswerId { get; set; }

        [ForeignKey("AnswerId")]
        public Answer Answer { get; set; }

        public DateTime? DateTimeBegin { get; set; }

        public DateTime? DateTimeEnd { get; set; }

        public ExamDetail()
        {
        }

        public ExamDetail(long exmaId, long serialNumber, long questionId)
        {
            ExamId = exmaId;
            SerialNumber = serialNumber;
            QuestionId = questionId;
        }

        public void Update(Answer answer)
        {
            Answer = answer;
            IsFinished = true;
            DateTimeEnd = DateTime.Now;

            if (answer is not null)
                IsCorrectlyAnswered = answer.IsRight;
        }
    }
}

