﻿namespace CsJobHunter.Core.Models.ExamAggregate
{
    public enum Mark
    {
        None,
        Fail,
        Bad,
        Satisfactory,
        Good,
        Excellent,
        Incomplete
    };
}

