﻿using RabbitMQ.Client;
using System.Text;

namespace CsJobHunter.Core.Models.ExamAggregate
{
    public class Client
    {
        private readonly string _hostName;
        private readonly string _userName;
        private readonly string _password;
        private readonly int _port;
        private readonly ConnectionFactory factory;

        public Client(string hostName, string userName, string password, int port = 5672)
        {
            this._hostName = hostName;
            this._userName = userName;
            this._password = password;
            this._port = port;

            this.factory = new ConnectionFactory()
            {
                HostName = _hostName,
                UserName = _userName,
                Password = _password,
                Port = _port
            };
        }

        public void Send(string message, string queue = "UsersTests")
        {
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                     routingKey: queue,
                                     basicProperties: null,
                                     body: body);
            }
        }
    }
}