﻿using CsJobHunter.Core.Models.Constants.ExamAggregate;
using CsJobHunter.Core.Models.StudentAggregate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CsJobHunter.Core.Models.ExamAggregate
{
    public class Exam : Entity
    {
        public DateTime DateTimeBegin { get; set; } = DateTime.Now;
        public DateTime? DateTimeEnd { get; set; }

        public Mark Mark { get; set; }

        public bool IsFinished { get; set; }
        public bool IsPassed { get; set; }

        public Status Status { get; set; }

        [Required]
        public long StudentId { get; set; }

        [Required]
        public virtual Student Student { get; set; }

        public ICollection<ExamDetail> ExamDetails { get; set; }

        public void Update(bool isFinished)
        {
            IsPassed = ExamDetails.All(e => e.IsCorrectlyAnswered == true);

            IsFinished = isFinished;

            if (IsFinished)
                DateTimeEnd = DateTime.Now;
        }
    }
}