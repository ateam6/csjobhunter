﻿namespace CsJobHunter.Core.Models.Constants.ExamAggregate
{
    public enum Status
    {
        None,
        Created,
        InProgress,
        Finished,
        Canceled
    }
}
