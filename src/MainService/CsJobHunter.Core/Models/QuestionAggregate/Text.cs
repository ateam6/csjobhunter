﻿using CSharpFunctionalExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CsJobHunter.Core.Models.QuestionAggregate
{
    public class Text : ValueObject
    {
        [Required]
        public string Value { get; }

        protected Text()
        {
        }
        protected Text(string value) : this()
        {
            Value = value;
        }

        protected static bool IsValid(string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value)) return false;
                if (string.IsNullOrWhiteSpace(value)) return false;
                value = value.Trim();
                if (value.Length > 256)
                    return false;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Result<Text> Create(string value)
        {
            if (!IsValid(value))
                return Result.Failure<Text>("Question text is not valid");

            return Result.Success(new Text(value.Trim()));
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
