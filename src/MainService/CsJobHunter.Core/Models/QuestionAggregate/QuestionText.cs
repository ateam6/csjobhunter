﻿using CSharpFunctionalExtensions;

namespace CsJobHunter.Core.Models.QuestionAggregate
{
    public class QuestionText : Text
    {
        protected QuestionText()
        {
        }

        private QuestionText(string value) : base(value)
        {
        }

        public static Result<QuestionText> Create(string value)
        {
            if (!IsValid(value))
                return Result.Failure<QuestionText>("Question text is not valid");

            return Result.Success(new QuestionText(value.Trim()));
        }
    }
}
