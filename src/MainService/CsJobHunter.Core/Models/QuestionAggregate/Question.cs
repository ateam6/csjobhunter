﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CsJobHunter.Core.Models.QuestionAggregate
{
    public class Question : Entity
    {
        [Required, MaxLength(256)]
        public virtual QuestionText Text { get; private set; }

        [MaxLength(256)]
        public virtual Code Code { get; private set; }

        public virtual ICollection<Answer> Answers { get; private set; }

        protected Question()
        {
        }

        private Question(QuestionText text, Code code = default)
        {
            Text = text;
            Code = code;
        }

        public void Update(QuestionText text, Code code)
        {
            Text = text ?? throw new ArgumentNullException(nameof(text));
            Code = code;
        }

        public override string ToString()
        {
            string result = $"  Id:{Id}   {Text.Value}";
            if (Code?.Value.Length > 0)
                result += "\n" + Code.Value;

            return result;
        }

        public static readonly string TheListOfAnswersIsNull = "The list of answers is null.";
        public static readonly string InAnswersListAbsentRightAnswer = "In answers list absent right answer.";
        public static readonly string InAnswersListMoreThanOneRightAnswer = "In answers list more than one right answer.";
        public static readonly string InAnswersListNonOrOnlyOneAnswer = "In answers list non or only one answer.";

        public void SetAnswers(List<AnswerDescription> answers)
        {
            if (answers == null)
                throw new ArgumentException(TheListOfAnswersIsNull);

            if (answers.Count <= 1)
                throw new ArgumentException(InAnswersListNonOrOnlyOneAnswer);

            var rightAnswersNum = answers.Where(a => a.IsRight == true).Count();

            if (rightAnswersNum == 0)
                throw new ArgumentException(InAnswersListAbsentRightAnswer);

            if (rightAnswersNum > 1)
                throw new ArgumentException(InAnswersListMoreThanOneRightAnswer);

            Answers = answers
                .Select(ans => new Answer(AnswerText.Create(ans.Text).Value, ans.IsRight, this))
                .ToList();
        }

        public static Question Create(string text, string code, List<AnswerDescription> answers)
        {
            Question question;

            if (code != null)
                question = new Question(QuestionText.Create(text).Value, Code.Create(code).Value);
            else
                question = new Question(QuestionText.Create(text).Value);

            question.SetAnswers(answers);

            return question;
        }
    }
}