﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CsJobHunter.Core.Models.QuestionAggregate
{
    public class Answer : Entity
    {
        private static readonly string _questionValueNullInConstructor = "Нулевое значение question в конструкторе Answer";

        [Required, MaxLength(256)]
        public virtual AnswerText Text { get; private set; }

        public bool IsRight { get; private set; }

        public long QuestionId { get; set; }

        [Required]
        public Question Question { get; set; }

        protected Answer()
        {
        }

        public Answer(AnswerText text, bool isRight, Question question)
        {
            if (question != null)
            {
                Text = text;
                IsRight = isRight;
                Question = question;
            }
            else
                throw new ArgumentException(_questionValueNullInConstructor);
        }

        public void Update(string text, bool isRight)
        {
            Text = text != null ? AnswerText.Create(text).Value : throw new ArgumentNullException(nameof(text));
            IsRight = isRight;
        }

        public override string ToString()
        {
            if (IsRight)
                return $"Id:{Id}  V   {Text.Value}";
            else
                return $"Id:{Id}  x   {Text.Value}";
        }
    }
}

