﻿namespace CsJobHunter.Core.Models.QuestionAggregate
{
    public class AnswerDescription
    {
        public string Text { get; set; }
        public bool IsRight { get; set; }
    }
}
