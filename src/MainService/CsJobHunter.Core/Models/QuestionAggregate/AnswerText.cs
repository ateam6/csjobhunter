﻿using CSharpFunctionalExtensions;

namespace CsJobHunter.Core.Models.QuestionAggregate
{
    public class AnswerText : Text
    {
        protected AnswerText()
        {
        }

        private AnswerText(string value) : base(value)
        {
        }

        new public static Result<AnswerText> Create(string value)
        {
            if (!IsValid(value))
                return Result.Failure<AnswerText>("Answer text is not valid");

            return Result.Success(new AnswerText(value.Trim()));
        }
    }
}
