﻿using CSharpFunctionalExtensions;
using System.Collections.Generic;

namespace CsJobHunter.Core.Models.QuestionAggregate
{
    public class Code : ValueObject
    {
        public string Value { get; }

        protected Code()
        {
        }
        private Code(string value) : this()
        {
            Value = value;
        }

        private static bool IsValid(string value)
        {
            if (value == null) return true;
            if (string.IsNullOrEmpty(value)) return false;
            if (string.IsNullOrWhiteSpace(value)) return false;
            value = value.Trim();
            if (value.Length > 256)
                return false;

            return true;
        }

        public static Result<Code> Create(string value)
        {
            if (!Code.IsValid(value))
                return Result.Failure<Code>("Code text is not valid");

            return Result.Success(new Code(value.Trim()));
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
