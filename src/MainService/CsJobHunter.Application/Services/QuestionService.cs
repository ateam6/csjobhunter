﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Questions;
using CsJobHunter.Core.Exceptions;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.QuestionAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CsJobHunter.Application.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;

        public QuestionService(
            IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }

        public async Task<QuestionResponse> GetByIdAsync(long id)
        {
            var question = await _questionRepository.GetByIdAsync(id);

            if (question?.Answers == null)
                throw new EntityNotFoundException(typeof(Question), id);

            return new QuestionResponse(question);
        }

        public async Task UpdateAsync(long id, QuestionAddUpdateRequest request)
        {
            var question = await _questionRepository.GetByIdAsync(id);

            if (question == null)
                throw new EntityNotFoundException(typeof(Question), id);

            var textResult = QuestionText.Create(request.QuestionText);
            if (textResult.IsFailure)
                throw new ArgumentException(textResult.Error);

            if (request.Code == null)
                question.Update(textResult.Value, null);
            else
            {
                var codeResult = Code.Create(request.Code);
                if (codeResult.IsFailure)
                    throw new ArgumentException(codeResult.Error);

                question.Update(textResult.Value, codeResult.Value);
            }

            // Т.к. кол-во ответов может измениться. Не стал усложнять код, меняя поштучно для каждого ответа.
            question.SetAnswers(ListAnswerDtoToListAnswerDescription(request.Answers));

            await _questionRepository.SaveChangesAsync();
        }

        public async Task<long> CreateAsync(QuestionAddUpdateRequest request)
        {
            var answers = ListAnswerDtoToListAnswerDescription(request.Answers);

            var question = Question.Create(request.QuestionText, request.Code, answers);

            await _questionRepository.InsertAsync(question);
            await _questionRepository.SaveChangesAsync();

            return question.Id;
        }

        public async Task DeleteAsync(long id)
        {
            await _questionRepository.RemoveAsync(id);

            await _questionRepository.SaveChangesAsync();
        }

        private static List<AnswerDescription> ListAnswerDtoToListAnswerDescription(List<AnswerDto> answers)
        {
            return answers
                .Select(ans => new AnswerDescription() { Text = ans.Text, IsRight = ans.IsRight })
                .ToList();
        }
    }
}
