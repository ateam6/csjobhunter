﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Exams;
using CsJobHunter.Core.Exceptions;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.ExamAggregate;
using CsJobHunter.Core.Models.QuestionAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CsJobHunter.Application.Services
{
    public class ExamDetailsService : IExamDetailsService
    {
        private readonly IRepository<Exam> _examRepository;
        private readonly IExamDetailsRepository _examDetailsRepository;
        private readonly IRepository<Question> _questionRepository;
        private readonly IRepository<Answer> _answerRepository;

        public ExamDetailsService(
            IExamDetailsRepository examDetailsRepository,
            IRepository<Exam> examRepository,
            IRepository<Answer> answerRepository,
            IRepository<Question> questionRepository)
        {
            _examDetailsRepository = examDetailsRepository;
            _examRepository = examRepository;
            _questionRepository = questionRepository;
            _answerRepository = answerRepository;
        }

        public async Task<IEnumerable<ExamWithDetailResponse>> GetAllByExamIdAsync(long examId)
        {
            var examDetails = await _examDetailsRepository.GetAllByExamIdAsync(examId);

            var examDetailResponse = examDetails.Select(e => new ExamWithDetailResponse(e));

            return examDetailResponse;
        }

        public async Task<ExamWithDetailResponse> GetByIdAsync(long id)
        {
            var examDetails = await _examDetailsRepository.GetByIdAsync(id);

            if (examDetails == null)
                throw new EntityNotFoundException(typeof(ExamDetail), id);

            return new ExamWithDetailResponse(examDetails);
        }

        public async Task<GetNextResponse> GetNextByExamIdAsync(long examId)
        {
            var examDetails =
                await _examDetailsRepository.GetNextByExamIdAsync(examId);

            GetNextResponse getNextResponse;

            if (examDetails == null)
            {
                getNextResponse = new GetNextResponse();
                getNextResponse.RemainCount = 0;
            }
            else
            {
                getNextResponse = new GetNextResponse(examDetails);

                getNextResponse.RemainCount =
                    await _examDetailsRepository.GetCountRemainIdAsync(examId);
            }

            return getNextResponse;
        }

        public async Task<bool> CreateAsync(CreateExamDetailsRequest createExamDetailsRequest)
        {
            var exam = await _examRepository.GetByIdAsync(createExamDetailsRequest.ExamId);

            if (exam is null)
                throw new EntityNotFoundException(typeof(Exam));

            var question = await _questionRepository.GetByIdAsync(createExamDetailsRequest.QuestionId);
            if (question is null)
                throw new EntityNotFoundException(typeof(Question));

            var examDetail = new ExamDetail()
            {
                ExamId = exam.Id,
                QuestionId = question.Id,
                AnswerId = 1,
                SerialNumber = createExamDetailsRequest.SerialNumber,
                IsFinished = createExamDetailsRequest.IsFinished
            };

            await _examDetailsRepository.InsertAsync(examDetail);

            await _examDetailsRepository.SaveChangesAsync();

            return examDetail.Id > 0;
        }

        public async Task<bool> UpdateAsync(UpdateExamDetailsRequest updateExamDetailsRequest)
        {
            var examDetail = await _examDetailsRepository.GetByIdAsync(updateExamDetailsRequest.Id);

            if (examDetail is null)
                throw new EntityNotFoundException(typeof(ExamDetail), updateExamDetailsRequest.Id);

            var exam = await _examRepository.GetByIdAsync(updateExamDetailsRequest.ExamId);
            if (exam is null)
                throw new EntityNotFoundException(typeof(Exam));

            var question = await _questionRepository.GetByIdAsync(updateExamDetailsRequest.QuestionId);
            if (question is null)
                throw new EntityNotFoundException(typeof(Question));

            var answer = _answerRepository.GetByIdAsync(updateExamDetailsRequest.AnswerId);

            examDetail.Update(answer.Result);

            await _examDetailsRepository.SaveChangesAsync();

            return true;
        }
    }
}
