﻿using CSharpFunctionalExtensions;
using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos.Students;
using CsJobHunter.Core.Exceptions;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.StudentAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CsJobHunter.Application.Services
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _studentRepository;

        public StudentService(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public async Task<IEnumerable<StudentResponse>> GetAllAsync()
        {
            var students = await _studentRepository.GetAllAsync();

            if (students == null)
                throw new EntityNotFoundException(typeof(Student));

            var studentResponse = students
                .Select(s => new StudentResponse(s));

            return studentResponse;
        }

        public async Task<StudentResponse> GetByIdAsync(long id)
        {
            var student = await _studentRepository.GetByIdAsync(id);

            if (student == null)
                throw new EntityNotFoundException(typeof(Student), id);

            return new StudentResponse(student);
        }


        public async Task<long> RegisterAsync(StudentRegisterRequest request)
        {
            var nameResult = Name.Create(request.FirstName, request.LastName);
            var loginResult = Login.Create(request.Login);
            var emailResult = Email.Create(request.Email);
            var passwordResult = Password.Create(request.Password);

            var result = Result.Combine(nameResult, emailResult, passwordResult, loginResult);

            if (result.IsFailure)
                throw new ArgumentException(result.Error);

            if (await _studentRepository.GetByLoginAsync(loginResult.Value.Value) != null)
                throw new ArgumentException("Login is already in use, select another one.");

            var student = new Student(loginResult.Value, nameResult.Value, emailResult.Value, passwordResult.Value);

            await _studentRepository.InsertAsync(student);
            await _studentRepository.SaveChangesAsync();

            return student.Id;
        }

        public async Task UpdateAsync(StudentUpdateRequest request)
        {
            var student = await _studentRepository.GetByIdAsync(request.Id);

            if (student == null)
                throw new EntityNotFoundException(typeof(Student), request.Id);


            var nameResult = Name.Create(request.FirstName, request.LastName);
            var emailResult = Email.Create(request.Email);

            var result = Result.Combine(nameResult, emailResult);

            if (result.IsFailure)
                throw new ArgumentException(result.Error);

            student.Update(nameResult.Value, emailResult.Value);

            await _studentRepository.SaveChangesAsync();
        }

        public async Task ChangePasswordAsync(StudentChangePasswordRequest request)
        {
            var student = await _studentRepository.GetByIdAsync(request.Id);

            if (student == null)
                throw new EntityNotFoundException(typeof(Student), request.Id);

            var passwordResult = Password.Create(request.Password);
            if (passwordResult.IsFailure)
                throw new ArgumentException(passwordResult.Error);

            student.ChangePassword(passwordResult.Value);

            await _studentRepository.SaveChangesAsync();
        }

        public async Task DeleteAsync(long id)
        {
            await _studentRepository.RemoveAsync(id);

            await _studentRepository.SaveChangesAsync();
        }
    }
}