﻿using CsJobHunter.Application.Contracts;
using CsJobHunter.Application.Dtos;
using CsJobHunter.Application.Dtos.Exams;
using CsJobHunter.Core.Exceptions;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.ExamAggregate;
using CsJobHunter.Core.Models.StudentAggregate;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CsJobHunter.Application.Services
{
    public class ExamService : IExamService
    {
        private readonly IExamRepository _examRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IExamDetailsRepository _examDetailsRepository;
        private readonly IStudentRepository _studentRepository;

        public ExamService(
            IExamRepository examRepository,
            IExamDetailsRepository examDetailsRepository,
            IQuestionRepository questionRepository,
            IStudentRepository studentRepository)
        {
            _examRepository = examRepository;
            _examDetailsRepository = examDetailsRepository;
            _questionRepository = questionRepository;
            _studentRepository = studentRepository;
        }

        public async Task<IEnumerable<ExamResponse>> GetAllAsync()
        {
            var exams = await _examRepository.GetAllAsync();

            var examsList = exams.ToList();

            IEnumerable<ExamResponse> GetListExamsResponses()
            {
                foreach (var e in examsList)
                    yield return new ExamResponse(e);
            }

            return GetListExamsResponses();
        }

        public async Task<ExamResponse> GetByIdAsync(long id)
        {
            var exam = await _examRepository.GetByIdAsync(id);

            if (exam == null)
                throw new EntityNotFoundException(typeof(Exam), id);

            return new ExamResponse(exam);
        }
        public async Task<IEnumerable<ExamResponse>> GetAllByStudentIdAsync(long studentId)
        {
            var exams = await _examRepository.GetAllByStudentIdAsync(studentId);

            return exams.Select(e => new ExamResponse(e));
        }

        public async Task<long> CreateAsync(CreateExamRequest createExamRequest)
        {
            var exam = new Exam() { StudentId = createExamRequest.StudentId };

            await _examRepository.InsertAsync(exam);

            await _examRepository.SaveChangesAsync();

            var questions = await _questionRepository.GetRandomSetOfQuestionsAsync(5);

            var questionNumber = 1;
            foreach (var question in questions)
            {
                await _examDetailsRepository.InsertAsync(new ExamDetail(exam.Id, questionNumber++, question.Id));
            }

            await _examDetailsRepository.SaveChangesAsync();

            return exam.Id;
        }

        public async Task UpdateAsync(UpdateExamRequest updateExamRequest)
        {
            var exam = await _examRepository.GetByIdAsync(updateExamRequest.Id);

            if (exam == null)
                throw new EntityNotFoundException(typeof(Exam), updateExamRequest.Id);

            exam.Update(updateExamRequest.IsFinished);

            await _examRepository.SaveChangesAsync();
        }

        public async Task<EmailDto> GetEmailAsync(UpdateExamRequest updateExamRequest)
        {
            var student = await _studentRepository.GetByIdAsync(updateExamRequest.StudentId);

            if (student == null)
                throw new EntityNotFoundException(typeof(Student), updateExamRequest.StudentId);

            var exam = await _examRepository.GetByIdAsync(updateExamRequest.Id);

            if (exam == null)
                throw new EntityNotFoundException(typeof(Exam), updateExamRequest.Id);

            var emailDto = new EmailDto
            {
                Sender = "Test",
                Subject = "Exam",
                Body = "Exam with tests",
                Date = System.DateTime.Now,
                Addresses = new List<string>()
                {
                    "cs-serge1@yandex.ru",
                    "sergei@vniigis-ztk.ru"
                }
            };

            return emailDto;
        }
    }
}
