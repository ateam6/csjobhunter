using System;
using System.Collections.Generic;

namespace CsJobHunter.Application.Dtos
{
    public class EmailDto
    {
        public DateTime Date { get; set; }
        public string Sender { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<string> Addresses { get; set; }
    }
}
