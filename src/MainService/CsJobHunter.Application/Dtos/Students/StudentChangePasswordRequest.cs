﻿namespace CsJobHunter.Application.Dtos.Students
{
    public class StudentChangePasswordRequest
    {
        public long Id { get; set; }
        public string Password { get; set; }
    }
}