﻿using CsJobHunter.Core.Models.StudentAggregate;

namespace CsJobHunter.Application.Dtos.Students
{
    public class StudentResponse
    {
        public long Id { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string FullName { get; }
        public string Email { get; }
        public string Login { get; }

        public StudentResponse(Student student)
        {
            Id = student.Id;
            FirstName = student.Name.First;
            LastName = student.Name.Last;
            FullName = $"{student.Name.First} {student.Name.Last}";
            Email = student.Email.Value;
            Login = student.Login.Value;
        }
    }
}
