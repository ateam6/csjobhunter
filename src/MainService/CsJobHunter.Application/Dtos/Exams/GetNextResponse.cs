﻿using CsJobHunter.Core.Models.ExamAggregate;

namespace CsJobHunter.Application.Dtos.Exams
{
    public class GetNextResponse : ExamWithDetailResponse
    {
        public int RemainCount { get; set; }

        public GetNextResponse(ExamDetail examDetail) : base(examDetail)
        {
        }

        public GetNextResponse() : base()
        { }
    }
}