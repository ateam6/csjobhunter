﻿using CsJobHunter.Core.Models.QuestionAggregate;

namespace CsJobHunter.Application.Dtos.Exams
{
    public class ExamAnswerDto
    {
        public long Id { get; }
        public string Text { get; }

        public ExamAnswerDto(Answer answer)
        {
            Id = answer.Id;
            Text = answer.Text.Value;
        }
    }
}
