﻿using System.Collections.Generic;
using CsJobHunter.Core.Models.Constants.ExamAggregate;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace CsJobHunter.Application.Dtos.Exams
{
    public class UpdateExamRequest
    {
        [NotNull]
        [Display(Name = "Id")]
        public long Id { get; set; }
        public long StudentId { get; set; }

        [Required]
        [Display(Name = "Статус")]
        public Status Status { get; set; }
        public bool IsFinished { get; set; }

        public List<string> EmailAddresses { get; set; }
    }
}
