﻿using CsJobHunter.Core.Models.Constants.ExamAggregate;
using CsJobHunter.Core.Models.ExamAggregate;
using CsJobHunter.Core.Models.StudentAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace CsJobHunter.Application.Dtos.Exams
{
    public class ExamResponse
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public string StudentFio { get; set; }

        public DateTime DateTimeBegin { get; set; }
        public DateTime? DateTimeEnd { get; set; }

        public bool IsFinished { get; set; }
        public bool IsPassed { get; set; }

        public Mark Mark { get; set; }
        public Status Status { get; set; }

        public int QuestionsCount { get; set; }
        public int PassedQuestionsCount { get; set; }

        [JsonIgnore]
        public Student Student { get; set; }

        [JsonIgnore]
        public List<ExamDetail> ExamDetails { get; set; }

        public ExamResponse(Exam exam)
        {
            Id = exam.Id;
            DateTimeBegin = exam.DateTimeBegin;
            DateTimeEnd = exam.DateTimeEnd;
            Mark = exam.Mark;
            IsFinished = exam.IsFinished;
            IsPassed = exam.IsPassed;
            StudentId = exam.StudentId;
            Student = exam.Student;
            StudentFio = $"{exam.Student?.Name.First} {exam.Student?.Name.Last}";
            ExamDetails = exam.ExamDetails?.ToList();
            QuestionsCount = exam.ExamDetails?.Count ?? 0;
            PassedQuestionsCount = exam.ExamDetails?.Where(e => e.IsCorrectlyAnswered).Count() ?? 0;
        }
    }
}
