﻿using CsJobHunter.Core.Models.ExamAggregate;
using CsJobHunter.Core.Models.QuestionAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace CsJobHunter.Application.Dtos.Exams
{
    public class ExamWithDetailResponse
    {
        public long Id { get; set; }

        public long ExamId { get; set; }

        public long SerialNumber { get; set; }

        public bool IsFinished { get; set; }

        public DateTime? DateTimeBegin { get; set; }

        public DateTime? DateTimeEnd { get; set; }

        public long QuestionId { get; set; }
        public string QuestionText { get; set; }

        public long? AnswerId { get; set; }

        [JsonIgnore]
        public Answer Answer { get; set; }
        public List<ExamAnswerDto> Answers { get; set; }

        public ExamWithDetailResponse(ExamDetail examDetail)
        {
            Id = examDetail.Id;
            ExamId = examDetail.ExamId;
            SerialNumber = examDetail.SerialNumber;
            IsFinished = examDetail.IsFinished;
            DateTimeBegin = examDetail.DateTimeBegin;
            DateTimeEnd = examDetail.DateTimeEnd;
            QuestionId = examDetail.QuestionId;
            AnswerId = examDetail.Answer?.Id;
            Answer = examDetail.Answer;
            Answers = examDetail.Question?.Answers?.Select(a => new ExamAnswerDto(a)).ToList();
            QuestionText = examDetail.Question?.Text.Value;
        }

        public ExamWithDetailResponse()
        { }
    }
}
