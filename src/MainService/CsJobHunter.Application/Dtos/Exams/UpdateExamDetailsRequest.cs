﻿namespace CsJobHunter.Application.Dtos.Exams
{
    public class UpdateExamDetailsRequest
    {
        public long Id { get; set; }
        public long SerialNumber { get; set; }
        public bool IsFinished { get; set; }
        public long ExamId { get; set; }
        public long QuestionId { get; set; }
        public long AnswerId { get; set; }
    }
}
