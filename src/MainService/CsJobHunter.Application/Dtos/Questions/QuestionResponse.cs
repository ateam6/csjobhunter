﻿using CsJobHunter.Core.Models.QuestionAggregate;
using System.Collections.Generic;
using System.Linq;

namespace CsJobHunter.Application.Dtos.Questions
{
    public class QuestionResponse
    {
        public string Text { get; }
        public string Code { get; }
        public List<AnswerResponse> Answers { get; }

        public QuestionResponse(Question question)
        {
            Text = question.Text.Value;
            Code = question.Code?.Value;

            Answers = question.Answers
                .Select(ans => new AnswerResponse(ans))
                .ToList();
        }
    }
}
