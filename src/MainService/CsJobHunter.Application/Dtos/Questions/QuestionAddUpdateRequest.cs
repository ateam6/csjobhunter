﻿using System.Collections.Generic;

namespace CsJobHunter.Application.Dtos.Questions
{
    public class QuestionAddUpdateRequest
    {
        public string QuestionText { get; set; }
        public string Code { get; set; }
        public List<AnswerDto> Answers { get; set; }        
    }
}
