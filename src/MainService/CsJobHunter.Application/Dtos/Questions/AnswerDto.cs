﻿namespace CsJobHunter.Application.Dtos.Questions
{
    public class AnswerDto
    {
        public string Text { get; set; }
        public bool IsRight { get; set; }
    }
}
