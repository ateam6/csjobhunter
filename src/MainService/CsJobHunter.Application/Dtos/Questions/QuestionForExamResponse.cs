﻿using CsJobHunter.Core.Models.QuestionAggregate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CsJobHunter.Application.Dtos.Questions
{
    public class QuestionForExamResponse
    {
        public int Number { get; }
        public bool IsLast { get; }
        public string Text { get; }
        public string Code { get; }
        public List<AnswerResponse> Answers { get; }

        public QuestionForExamResponse(Question question, int number, bool isLast)
        {
            Number = number;
            IsLast = isLast;

            Text = question.Text.Value;
            Code = question.Code?.Value;

            Answers = new List<AnswerResponse>();

            var srcAnswersList = question.Answers.ToList();

            Random rnd = new();
            while (srcAnswersList.Count > 0)
            {
                int i = rnd.Next(srcAnswersList.Count);
                Answers.Add(new AnswerResponse(srcAnswersList[i]));
                srcAnswersList.RemoveAt(i);
            }
        }
    }
}
