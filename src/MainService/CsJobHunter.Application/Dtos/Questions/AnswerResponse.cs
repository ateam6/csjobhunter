﻿using CsJobHunter.Core.Models.QuestionAggregate;

namespace CsJobHunter.Application.Dtos.Questions
{
    public class AnswerResponse
    {
        public long Id { get; }
        public string Text { get; }
        public bool IsRight { get; }

        public AnswerResponse(Answer answer)
        {
            Id = answer.Id;
            Text = answer.Text.Value;
            IsRight = answer.IsRight;
        }
    }
}
