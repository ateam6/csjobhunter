﻿using CsJobHunter.Application.Dtos.Students;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsJobHunter.Application.Contracts
{
    public interface IStudentService
    {
        Task<IEnumerable<StudentResponse>> GetAllAsync();
        Task<StudentResponse> GetByIdAsync(long id);
        Task<long> RegisterAsync(StudentRegisterRequest request);
        Task UpdateAsync(StudentUpdateRequest request);
        Task ChangePasswordAsync(StudentChangePasswordRequest request);
        Task DeleteAsync(long id);
    }
}
