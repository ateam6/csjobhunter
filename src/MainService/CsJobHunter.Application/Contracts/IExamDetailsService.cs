﻿using CsJobHunter.Application.Dtos.Exams;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsJobHunter.Application.Contracts
{
    public interface IExamDetailsService
    {
        Task<IEnumerable<ExamWithDetailResponse>> GetAllByExamIdAsync(long examId);
        Task<ExamWithDetailResponse> GetByIdAsync(long id);
        Task<GetNextResponse> GetNextByExamIdAsync(long examId);
        Task<bool> CreateAsync(CreateExamDetailsRequest createExamDetailsRequest);
        Task<bool> UpdateAsync(UpdateExamDetailsRequest updateExamDetailsRequest);
    }
}
