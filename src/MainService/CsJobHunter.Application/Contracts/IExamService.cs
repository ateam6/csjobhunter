﻿using CsJobHunter.Application.Dtos;
using CsJobHunter.Application.Dtos.Exams;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsJobHunter.Application.Contracts
{
    public interface IExamService
    {
        Task<IEnumerable<ExamResponse>> GetAllAsync();
        Task<ExamResponse> GetByIdAsync(long id);
        Task<EmailDto> GetEmailAsync(UpdateExamRequest updateExamRequest);
        Task<IEnumerable<ExamResponse>> GetAllByStudentIdAsync(long id);
        Task<long> CreateAsync(CreateExamRequest createExamRequest);
        Task UpdateAsync(UpdateExamRequest updateExamRequest);
    }
}