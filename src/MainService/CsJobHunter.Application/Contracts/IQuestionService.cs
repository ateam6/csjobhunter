﻿using CsJobHunter.Application.Dtos.Questions;
using System.Threading.Tasks;

namespace CsJobHunter.Application.Contracts
{
    public interface IQuestionService
    {
        Task<QuestionResponse> GetByIdAsync(long id);
        Task<long> CreateAsync(QuestionAddUpdateRequest request);
        Task UpdateAsync(long id, QuestionAddUpdateRequest request);
        Task DeleteAsync(long id);
    }
}
