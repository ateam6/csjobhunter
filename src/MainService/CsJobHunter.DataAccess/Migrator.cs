﻿using CsJobHunter.DataAccess.Seed;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace CsJobHunter.DataAccess
{
    public static class Migrator
    {
        public static void MigrateAndSeedDb(this IApplicationBuilder app, bool development = false)
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<CsJobHunterDbContext>();

            // context.Migrate();
            if (development)
                context.Seed();
        }
        private static void Migrate(this CsJobHunterDbContext context)
        {
            if (context.Database.GetPendingMigrations().Any())
                context.Database.Migrate();
        }

        private static void Seed(this CsJobHunterDbContext context)
        {
            using var dbContext = context;
            if (dbContext.Questions.Any())
            {
                return;
            }
            dbContext.AddSeedData();
            dbContext.SaveChanges();
            dbContext.ShowData();
        }
    }
}
