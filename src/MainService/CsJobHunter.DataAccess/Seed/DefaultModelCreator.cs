using CsJobHunter.Core.Models.ExamAggregate;
using CsJobHunter.Core.Models.QuestionAggregate;
using CsJobHunter.Core.Models.StudentAggregate;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CsJobHunter.DataAccess.Seed
{
    public static class DefaultModelCreator
    {
        internal static CsJobHunterDbContext AddSeedData(this CsJobHunterDbContext dbContext)
        {
            if (!dbContext.Students.Any())
            {
                var students = new Student[]
                {
                    new Student (Login.Create("fvasiliy").Value, Name.Create("Василий", "Первый").Value, Email.Create("firstStudentTestEmail@gmail.com").Value, Password.Create( "sdfssdfsd1").Value),
                    new Student (Login.Create("spetr").Value, Name.Create("Пётр", "Второй").Value, Email.Create("secondStudentTestEmail@yandex.ru").Value, Password.Create("sfsdsdfff1").Value)
                };
                dbContext.Students.AddRange(students);
            }

            if (!dbContext.Questions.Any())
                AddQuestionsAndTheirsAnswers(dbContext);

            dbContext.SaveChanges();

            if (!dbContext.Exams.Any() && !dbContext.ExamDetails.Any())
            {
                Student1ToPassExam(dbContext);
                Student2ToPassExam(dbContext);
                Student1ToPassAnotherExam(dbContext);
            }

            return dbContext;
        }

        public static void AddQuestionsAndTheirsAnswers(this CsJobHunterDbContext dbContext)
        {
            dbContext.Questions.AddRange(
                Question.Create("Что такое int?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Тип данных", IsRight = true },
                    new AnswerDescription() { Text = "Класс", IsRight = false },
                    new AnswerDescription() { Text = "Интерфейс", IsRight = false }
                }),


                Question.Create("Что будет содержать строка name после выполнения данного кода?",
                                "string name = \"Hello\";\nname[1] = '.';", new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "\"Hello\"", IsRight = false },
                    new AnswerDescription() { Text = "\"H.llo\"", IsRight = false },
                    new AnswerDescription() { Text = "\".ello\"", IsRight = false },
                    new AnswerDescription() { Text = "Код не скомпилируется", IsRight = true }
                }),

                Question.Create("Может ли класс реализовывать несколько интерфейсов?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Нет, в C# не поддерживается множественное наследование", IsRight = false },
                    new AnswerDescription() { Text = "Да может", IsRight = true }
                }),

             Question.Create("Какой уровень доступа имеют методы класса, если модификатор доступа не указан?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "protected", IsRight = false },
                    new AnswerDescription() { Text = "protected internal", IsRight = false },
                    new AnswerDescription() { Text = "internal", IsRight = false },
                    new AnswerDescription() { Text = "public", IsRight = false },
                    new AnswerDescription() { Text = "private", IsRight = true }
                }),

                Question.Create("Чему будет равно значение переменной str после выполнения операторов?",
                                "string str = \"Hello world\";\nstring s = str.Remove(5, 6)", new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "\"Hello\"", IsRight = false },
                    new AnswerDescription() { Text = "\"Hello world\"", IsRight = true },
                    new AnswerDescription() { Text = "Код не скомпилируется", IsRight = false }
                }),

                Question.Create("Что будет выведено при выполнении кода?",
                                "string a = \"hello\";\nstring b = \"h\";\nb += \"ello\";\nConsole.WriteLine(a == b);\nConsole.WriteLine(object.ReferenceEquals(a, b));", new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "true, false", IsRight = true },
                    new AnswerDescription() { Text = "true, true", IsRight = false },
                    new AnswerDescription() { Text = "false, false", IsRight = false }
                }),

                Question.Create("Возможно ли в C# множественное наследование классов?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Да", IsRight = false },
                    new AnswerDescription() { Text = "Нет", IsRight = true }
                }),

                Question.Create("Строки это:", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Ссылочный тип", IsRight = true },
                    new AnswerDescription() { Text = "Значимый тип", IsRight = false }
                }),

                Question.Create("Структуры это:", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Ссылочный тип", IsRight = false },
                    new AnswerDescription() { Text = "Значимый тип", IsRight = true }
                }),

                Question.Create("Может ли структура реализовывать несколько интерфейсов?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Нет, в C# запрещено множественное наследование", IsRight = false },
                    new AnswerDescription() { Text = "Может", IsRight = true },
                    new AnswerDescription() { Text = "Структура не может реализовывать интерфейсы и участвовать в наследовании", IsRight = false }
                }),

                Question.Create("Может ли структура содержать виртуальные методы?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Не может, т.к. структуры не могут участвовать в иерархиях наследования", IsRight = true},
                    new AnswerDescription() { Text = "Может", IsRight = false}
                }),

                Question.Create("Можно ли переменной типа byte присвоить значение 300?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Нельзя", IsRight = true},
                    new AnswerDescription() { Text = "Можно", IsRight = false},
                    new AnswerDescription() { Text = "Можно, если явно указать приведение типа", IsRight = false}
                }),

                Question.Create("Для чего служат операторы checked и unchecked?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Для управления процессом генерации исключений, возникающих при переполнении", IsRight = true},
                    new AnswerDescription() { Text = "Для пометки протестированных и непротестированных фрагментов кода", IsRight = false}
                }),

                Question.Create("Какой из операторов не относится к методам LINQ?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Where", IsRight = false},
                    new AnswerDescription() { Text = "Join", IsRight = false},
                    new AnswerDescription() { Text = "Add", IsRight = true},
                    new AnswerDescription() { Text = "Distinct", IsRight = false},
                }),

                Question.Create("Возможно ли в операторах LINQ использовать одновременно декларативный синтаксис и синтаксис методов?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Да", IsRight = true},
                    new AnswerDescription() { Text = "Нет", IsRight = false}
                }),

                Question.Create("С помощью какого ключевого слова осуществляется блокировка одновременного выполнения определенных участков кода несколькими потоками?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "checked", IsRight = false},
                    new AnswerDescription() { Text = "internal", IsRight = false},
                    new AnswerDescription() { Text = "safe", IsRight = false},
                    new AnswerDescription() { Text = "protected", IsRight = false},
                    new AnswerDescription() { Text = "lock", IsRight = true}
                }),

                Question.Create("Возможно ли определять лямбда выражение, состоящее из нескольких операторов?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Возможно", IsRight = true},
                    new AnswerDescription() { Text = "Нет", IsRight = false}
                }),

                Question.Create("Какие типы данных можно использовать в цикле foreach?\nВыберите верный и наиболее полный вариант ответа. ", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Массивы каких либо данных", IsRight = false},
                    new AnswerDescription() { Text = "Массивы и списки каких либо данных", IsRight = false},
                    new AnswerDescription() { Text = "Поддерживающие интерфейс IEnumerable", IsRight = true},
                    new AnswerDescription() { Text = "Поддерживающие интерфейс IForeach", IsRight = false},
                }),

                Question.Create("Может ли класс реализовать два интерфейса, у которых объявлены одинаковые методы?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Нет, в таких случаеях следует переименовать один из методов.", IsRight = false},
                    new AnswerDescription() { Text = "Может, при указании дополнительной информации в реализациях методов", IsRight = true}
                }),

                Question.Create("Наследуются ли переменные с модификатором private?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Нет, для унаследования переменных они должны иметь модификаторы public или protected", IsRight = true},
                    new AnswerDescription() { Text = "Да, но они не являются доступными вне класса наследника", IsRight = false}
                }),

                Question.Create("Блоки кода finally выполняются только в том случае, если не выполняется блок catch?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Да", IsRight = false},
                    new AnswerDescription() { Text = "В зависимости от кода", IsRight = false},
                    new AnswerDescription() { Text = "Нет, в любом случае", IsRight = true}
                }),

                Question.Create("Выполнится ли блок finally, если исключения в защищаемом блоке не было?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Зависит от кода", IsRight = false},
                    new AnswerDescription() { Text = "Нет", IsRight = false},
                    new AnswerDescription() { Text = "Зависит от режима работы", IsRight = false},
                    new AnswerDescription() { Text = "Да", IsRight = true}
                }),

                Question.Create("Можно ли в C# наследоваться от нескольких классов?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Да", IsRight = false},
                    new AnswerDescription() { Text = "Нет", IsRight = true}
                }),

                Question.Create("Что такое перегрузка методов?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Использование одного имени для разных методов", IsRight = true},
                    new AnswerDescription() { Text = "Передача в функцию данных размером более 1М", IsRight = false},
                    new AnswerDescription() { Text = "Возврат из функции оператором return данных размером более 1М", IsRight = false}
                }),

                Question.Create("Как определяются несоздаваемые классы?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Определением их статическими или объявлением всех конструкторов приватными", IsRight = true},
                    new AnswerDescription() { Text = "Модификатором sealed", IsRight = false},
                    new AnswerDescription() { Text = "Модификатором uncreated", IsRight = false}
                }),

                Question.Create("Что делает модификатор partial?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Позволяет разделять сущность на несколько частей", IsRight = true},
                    new AnswerDescription() { Text = "Позволяет использовать неуправляемый код", IsRight = false},
                    new AnswerDescription() { Text = "Запрещает наследование", IsRight = false},
                    new AnswerDescription() { Text = "Ограничивает видимость текущим классом", IsRight = false}
                }),

                Question.Create("Тип string является значимым или ссылочным типом?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Ссылочным", IsRight = true},
                    new AnswerDescription() { Text = "Значимым", IsRight = false},
                    new AnswerDescription() { Text = "В зависимости от кода", IsRight = false}
                }),

                Question.Create("Верно ли, что тип string хранит набор символов Unicode?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Да", IsRight = true},
                    new AnswerDescription() { Text = "Нет", IsRight = false}
                }),

                Question.Create("Что делает модификатор sealed?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Запрещает наследование", IsRight = true},
                    new AnswerDescription() { Text = "Добавляется просто для наглядности", IsRight = false},
                    new AnswerDescription() { Text = "Ограничивает видимость текущей сборкой", IsRight = false},
                    new AnswerDescription() { Text = "Разрешает наследование только один раз", IsRight = false}
                }),

                Question.Create("Что неверно в следующем коде?", "public class Instantiator<T>\n{\npublic T instance;\npublic Instantiator()\n{\ninstance = new T();\n}\n}", new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Cледует наложить ограничение на тип Т (where T: new())", IsRight = true},
                    new AnswerDescription() { Text = "Тип T используется слишком часто", IsRight = false}
                }),

                Question.Create("Что будет выведено на консоль, в результате работы следующего кода?", "List<Action> actions = new();\nfor (var count = 0; count < 10; count++)\n{\n    actions.Add(() => Console.WriteLine(count));\n}\nforeach (var action in actions)\n{\n    action();\n}", new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "10, 10, 10, 10, 10, 10, 10, 10, 10, 10", IsRight = true},
                    new AnswerDescription() { Text = "0, 1, 2, 3, 4, 5, 6, 7, 8, 9", IsRight = false},
                    new AnswerDescription() { Text = "0, 0, 0, 0, 0, 0, 0, 0, 0, 0", IsRight = false},
                    new AnswerDescription() { Text = "Код сгенерирует исключение", IsRight = false}
                }),

                Question.Create("Когда использовать StringBuilder предпочтительнее, чем string?", null, new List<AnswerDescription>()
                {
                    new AnswerDescription() { Text = "Если строка редко изменяется", IsRight = false},
                    new AnswerDescription() { Text = "Если строка содержит спецсимволы", IsRight = false},
                    new AnswerDescription() { Text = "Если строка часто изменяется", IsRight = true},
                    new AnswerDescription() { Text = "Если строка содержит исключительно цифры", IsRight = false}
                })
            );
        }

        private static void Student1ToPassExam(this CsJobHunterDbContext dbContext)
        {
            var student1 = dbContext.Students.FirstOrDefault();
            if (student1 != null)
            {
                DateTime examBegin = new(2021, 4, 3, 14, 04, 25); // год - месяц - день - час - минута - секунда.
                DateTime examEnd = new(2021, 4, 3, 14, 05, 29); // год - месяц - день - час - минута - секунда.
                Exam student1exam1 = new() { DateTimeBegin = examBegin, DateTimeEnd = examEnd, Student = student1, Mark = Mark.Incomplete, IsFinished = true };

                Question question1 = dbContext.Questions.First();  // без проверки.

                dbContext.Entry(question1).Collection("Answers").Load();
                Answer[] answers = question1.Answers.ToArray();

                // Пусть был выбран ответ №2 (не проверяется наличие ответа с этим индексом)                
                dbContext.ExamDetails.AddRange(new ExamDetail() { Exam = student1exam1, QuestionId = question1.Id, AnswerId = answers[1].Id, DateTimeBegin = examBegin.AddSeconds(12), DateTimeEnd = examBegin.AddSeconds(80) });

                student1exam1.DateTimeEnd = examBegin.AddMinutes(2);
                student1exam1.Mark = Mark.Fail;
                dbContext.Exams.AddRange(student1exam1);
            }
        }

        private static void Student2ToPassExam(this CsJobHunterDbContext dbContext)
        {
            var students = dbContext.Students.ToList();
            if (students.Count >= 2)
            {
                Student student2 = students[1];
                DateTime examBegin = new(2021, 4, 3, 15, 26, 25); // год - месяц - день - час - минута - секунда.
                Exam student2exam = new() { DateTimeBegin = examBegin, Student = student2, Mark = Mark.Incomplete };

                Question question1 = dbContext.Questions.First();  // без проверки.

                dbContext.Entry(question1).Collection("Answers").Load();
                Answer[] answers = question1.Answers.ToArray();

                // Пусть был выбран ответ №1 (не проверяется наличие ответа с этим индексом)
                dbContext.ExamDetails.AddRange(new ExamDetail() { Exam = student2exam, QuestionId = question1.Id, AnswerId = answers[0].Id, DateTimeBegin = examBegin.AddSeconds(10), DateTimeEnd = examBegin.AddSeconds(90) });

                Question question2 = dbContext.Questions.ToArray()[1];  // без проверки.

                dbContext.Entry(question2).Collection("Answers").Load();
                answers = question2.Answers.ToArray();

                // Пусть был выбран ответ №2 (не проверяется наличие ответа с этим индексом)
                dbContext.ExamDetails.AddRange(new ExamDetail() { Exam = student2exam, QuestionId = question2.Id, AnswerId = answers[1].Id, DateTimeBegin = examBegin.AddMinutes(3), DateTimeEnd = examBegin.AddSeconds(190) });

                student2exam.DateTimeEnd = examBegin.AddMinutes(2);
                student2exam.Mark = Mark.Excellent;
                dbContext.Exams.AddRange(student2exam);
            }
        }

        private static void Student1ToPassAnotherExam(this CsJobHunterDbContext dbContext)
        {
            var student1 = dbContext.Students.FirstOrDefault();
            if (student1 != null)
            {
                DateTime examBegin = new(2021, 4, 3, 15, 46, 25); // год - месяц - день - час - минута - секунда.
                Exam student1exam2 = new() { DateTimeBegin = examBegin, Student = student1, Mark = Mark.Incomplete };

                Question[] questions = dbContext.Questions.ToArray();

                if (questions.Length >= 4)
                {
                    Question question1 = questions[2];

                    dbContext.Entry(question1).Collection("Answers").Load();
                    Answer[] answers = question1.Answers.ToArray();

                    // Пусть был выбран ответ №2                
                    dbContext.ExamDetails.AddRange(new ExamDetail() { Exam = student1exam2, QuestionId = question1.Id, AnswerId = answers[1].Id, DateTimeBegin = examBegin.AddMinutes(1) });

                    Question question2 = questions[3];

                    dbContext.Entry(question2).Collection("Answers").Load();
                    answers = question2.Answers.ToArray();

                    // Пусть был выбран ответ №2                
                    dbContext.ExamDetails.AddRange(new ExamDetail() { Exam = student1exam2, QuestionId = question2.Id, AnswerId = answers[1].Id, DateTimeBegin = examBegin.AddMinutes(3) });

                    student1exam2.DateTimeEnd = examBegin.AddMinutes(2);
                    student1exam2.Mark = Mark.Satisfactory;
                    dbContext.Exams.AddRange(student1exam2);
                }
            }
        }

        internal static CsJobHunterDbContext ShowData(this CsJobHunterDbContext dbContext)
        {
            ShowStudentsAndTheirExams(dbContext);
            ShowQuestionsAndAnswersToThem(dbContext);

            return dbContext;
        }

        private static void ShowStudentsAndTheirExams(this CsJobHunterDbContext dbContext)
        {
            List<Student> students = dbContext.Students
                                        .Include(s => s.Exams)
                                        .ThenInclude(e => e.ExamDetails)
                                        .ToList();

            Console.WriteLine("Список студентов в таблице Students:");

            foreach (Student st in students)
            {
                System.Diagnostics.Debug.WriteLine("{0}.  {1} - Login: {2}  e-mail: {3}", st.Id, st.Name.First, st.Login.Value, st.Email.Value);

                if (st.Exams != null)
                {
                    foreach (var exam in st.Exams)
                    {
                        System.Diagnostics.Debug.WriteLine($"      {exam.Id}: Exam result = {exam.Mark}");
                        foreach (var examDet in exam.ExamDetails)
                        {
                            var question = dbContext.Questions.Find(examDet.QuestionId);
                            System.Diagnostics.Debug.WriteLine($"         QestionId: {examDet.QuestionId}  {question.Text.Value}");
                        }
                    }
                }
                else
                    System.Diagnostics.Debug.WriteLine("      у этого студента нет инф. о экзаменах");
            }
        }

        internal static CsJobHunterDbContext ClearDBTables(this CsJobHunterDbContext dbContext)
        {
            dbContext.Students.RemoveRange(dbContext.Students);
            dbContext.Exams.RemoveRange(dbContext.Exams);
            dbContext.ExamDetails.RemoveRange(dbContext.ExamDetails);
            dbContext.Questions.RemoveRange(dbContext.Questions);
            dbContext.SaveChanges();

            return dbContext;
        }

        private static void ShowQuestionsAndAnswersToThem(this CsJobHunterDbContext dbContext)
        {
            System.Diagnostics.Debug.WriteLine("\nОбщий список вопросов и ответов:");
            List<Question> questions = dbContext.Questions
                        .Include(p => p.Answers)
                        .ToList();

            foreach (Question qstn in questions)
            {
                System.Diagnostics.Debug.WriteLine(qstn);

                if (qstn.Answers?.Count > 0)
                {
                    foreach (var ans in qstn.Answers)
                        System.Diagnostics.Debug.WriteLine("   " + ans);
                }

                System.Diagnostics.Debug.WriteLine("-----------------------");
            }
        }
    }
}
