﻿using CsJobHunter.Core.Models.StudentAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CsJobHunter.DataAccess.Configuration
{
    public class StudentConfig
        : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.Property(p => p.Email)
                .HasConversion(p => p.Value, p => Email.Create(p).Value);

            builder.OwnsOne(p => p.Password, p =>
            {
                p.Property(pp => pp.Value).HasColumnName("Password");
            });

            builder.OwnsOne(p => p.Login, p =>
            {
                p.Property(pp => pp.Value).HasColumnName("Login");
                p.HasIndex("Value").IsUnique();
            });

            builder.OwnsOne(p => p.Name, p =>
            {
                p.Property(pp => pp.First).HasColumnName("FirstName");
                p.Property(pp => pp.Last).HasColumnName("LastName");
            });

            builder.Navigation(p => p.Name).IsRequired();
        }
    }
}
