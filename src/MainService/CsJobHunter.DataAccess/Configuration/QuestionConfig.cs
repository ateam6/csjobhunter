﻿using CsJobHunter.Core.Models.QuestionAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CsJobHunter.DataAccess.Configuration
{
    public class QuestionConfig : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder.Property(p => p.Text)
                .HasConversion(p => p.Value, p => QuestionText.Create(p).Value);

            builder.Property(p => p.Code)
                .HasConversion(p => p.Value, p => Code.Create(p).Value);
        }
    }
}
