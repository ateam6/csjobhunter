﻿using CsJobHunter.Core.Models.QuestionAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CsJobHunter.DataAccess.Configuration
{
    public class AnswerConfig : IEntityTypeConfiguration<Answer>
    {
        public void Configure(EntityTypeBuilder<Answer> builder)
        {
            builder.Property(p => p.Text)
                .HasConversion(p => p.Value, p => AnswerText.Create(p).Value);
        }
    }
}
