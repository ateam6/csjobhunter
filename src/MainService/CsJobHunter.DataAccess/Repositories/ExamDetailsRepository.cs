using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.ExamAggregate;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CsJobHunter.DataAccess.Repositories
{
    public class ExamDetailsRepository :
        BaseRepository<ExamDetail>, 
        IExamDetailsRepository
    {
        public ExamDetailsRepository(CsJobHunterDbContext context)
            : base(context)
        {
        }

        public async Task<IEnumerable<ExamDetail>> GetAllByExamIdAsync(long examId)
        {
            return await _context.ExamDetails
                .Include(e => e.Exam)
                .Where(e => e.Exam.Id == examId)
                .ToListAsync();
        }

        public async Task<ExamDetail> GetNextByExamIdAsync(long idExam)
        {
            return await _context.ExamDetails
                .Include(e => e.Question)
                    .ThenInclude(q => q.Answers)
                .FirstOrDefaultAsync(e => e.ExamId == idExam && !e.IsFinished);
        }

        public async Task<int> GetCountRemainIdAsync(long idExam)
        {
            return await _context.ExamDetails
                .Where(e => e.ExamId == idExam && !e.IsFinished).CountAsync();
        }
    }
}