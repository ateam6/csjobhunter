﻿using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.QuestionAggregate;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CsJobHunter.DataAccess.Repositories
{
    public class QuestionRepository : BaseRepository<Question>, IQuestionRepository
    {
        private readonly Random _rnd = new();
        protected static readonly string _questionsNumTooBig = "Параметр questionsNum при вызове IQuestionRepository.GetRandomSetOfQuestionsAsync() больше количества вопросов в базе";

        public QuestionRepository(CsJobHunterDbContext context)
            : base(context)
        {
        }

        public override async Task<Question> GetByIdAsync(long id)
        {
            var entity = await base.GetByIdAsync(id);

            await _context.Entry(entity).Collection(e => e.Answers).LoadAsync();

            return entity;
        }

        private async Task<List<long>> GetAllIdsAsync()
        {
            return await _context.Questions.Select(q => q.Id).ToListAsync();
        }

        public async Task<IEnumerable<Question>> GetRandomSetOfQuestionsAsync(byte questionsNumInSet)
        {
            var questionsIds = await GetAllIdsAsync();

            if (questionsIds.Count < questionsNumInSet)
                throw new ArgumentException(_questionsNumTooBig);

            var result = new List<Question>();

            if (questionsIds.Count <= 50)
            {
                // Из списка Id удаляется по одному случайному элементу, пока не останется questionsNum элементов.
                while (questionsIds.Count > questionsNumInSet)
                    questionsIds.RemoveAt(_rnd.Next(questionsIds.Count));

                for (ushort i = 0; i < questionsIds.Count; i++)
                    result.Add(await GetByIdAsync(questionsIds[i]));
            }
            else
            {
                // Из списка Id вытаскивается по одному случайному элементу, пока не наберётся questionsNum неповторяющихся элементов.
                // Если за MaximumAttemptsNumber таких случайных вытаскиваний список не наберётся -добирается последовательным проходом questionsIds.
                long maximumAttemptsNumber = 10 * questionsNumInSet;
                long attemptsNum = 0;
                var generatedIdsList = new List<long>();
                while (generatedIdsList.Count < questionsNumInSet && attemptsNum++ < maximumAttemptsNumber)
                {
                    long insId = questionsIds[_rnd.Next(questionsIds.Count)];
                    if (!generatedIdsList.Contains(insId))
                        generatedIdsList.Add(insId);
                }

                if (generatedIdsList.Count < questionsNumInSet)
                    // Добрать недостающие элементы последовательным проходом.
                    for (int i = 0; i < questionsIds.Count; i++)
                        if (!generatedIdsList.Contains(questionsIds[i]))
                        {
                            generatedIdsList.Add(questionsIds[i]);
                            if (generatedIdsList.Count >= questionsNumInSet)
                                break;
                        }

                for (ushort i = 0; i < generatedIdsList.Count; i++)
                    result.Add(await GetByIdAsync(generatedIdsList[i]));
            }

            return result;
        }
    }
}