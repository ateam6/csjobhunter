﻿using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.StudentAggregate;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CsJobHunter.DataAccess.Repositories
{
    public class StudentRepository
        : BaseRepository<Student>, IStudentRepository
    {
        public StudentRepository(CsJobHunterDbContext context)
            : base(context)
        {
        }

        public async Task<IEnumerable<Student>> GetAllAsync()
        {
            return await _context.Set<Student>().ToListAsync();
        }


        public override async Task<Student> GetByIdAsync(long id)
        {
            var entity = await base.GetByIdAsync(id);

            await _context.Entry(entity).Collection(e => e.Exams).LoadAsync();

            return entity;
        }

        public async Task<Student> GetByLoginAsync(string login)
        {
            return await _context.Students.Where(e => e.Login.Value == login).FirstOrDefaultAsync();
        }
    }
}
