﻿using System.Collections.Generic;
using System.Linq;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models.ExamAggregate;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CsJobHunter.DataAccess.Repositories
{
    public class ExamRepository :
        BaseRepository<Exam>, IExamRepository
    {
        public ExamRepository(CsJobHunterDbContext context)
            : base(context)
        {
        }

        public override async Task<Exam> GetByIdAsync(long id)
        {
            return await _context.Exams
                .Include(e => e.ExamDetails)
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<IEnumerable<Exam>> GetAllByStudentIdAsync(long id)
        {
            return await _context.Exams
                .Include(e => e.ExamDetails)
                .Where(e => e.StudentId == id)
                .ToListAsync();
        }
    }
}
