﻿using CsJobHunter.Core.Exceptions;
using CsJobHunter.Core.Interfaces;
using CsJobHunter.Core.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CsJobHunter.DataAccess.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : Entity
    {
        protected readonly CsJobHunterDbContext _context;

        public BaseRepository(CsJobHunterDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public virtual async Task<TEntity> GetByIdAsync(long id)
        {
            var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(e => e.Id == id);

            return entity;
        }

        public virtual async Task InsertAsync(TEntity entity)
        {
            await Task.FromResult(_context.Set<TEntity>().Attach(entity));
        }

        public virtual async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task RemoveAsync(long id)
        {
            var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(e => e.Id == id);

            if (entity == null)
                return;

            _context.Remove(entity);
        }
    }
}