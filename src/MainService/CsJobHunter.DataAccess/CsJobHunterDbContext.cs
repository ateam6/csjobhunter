﻿using CsJobHunter.Core.Models.ExamAggregate;
using CsJobHunter.Core.Models.QuestionAggregate;
using CsJobHunter.Core.Models.StudentAggregate;
using CsJobHunter.DataAccess.Configuration;
using Microsoft.EntityFrameworkCore;

namespace CsJobHunter.DataAccess
{
    public class CsJobHunterDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<ExamDetail> ExamDetails { get; set; }

        protected CsJobHunterDbContext()
        {
        }

        public CsJobHunterDbContext(DbContextOptions<CsJobHunterDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new StudentConfig())
                        .ApplyConfiguration(new QuestionConfig())
                        .ApplyConfiguration(new AnswerConfig());
        }
    }
}
