﻿namespace CsJobHunter.IdentityServer.Options
{
    public class ExternalUrlOptions
    {
        public const string ExternalUrls = "ExternalUrls";

        public string WebApi { get; set; }
        public string Authority { get; set; }
    }
}
