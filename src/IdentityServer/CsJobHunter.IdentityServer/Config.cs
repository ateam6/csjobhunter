﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using CsJobHunter.IdentityServer.Options;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace CsJobHunter.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("exam.scope"),
                new ApiScope("emailsending.scope"),
            };

        public static IEnumerable<Client> GetClients(ExternalUrlOptions externalUrls) =>
            new Client[]
            {
                // m2m client credentials flow client
                new Client
                {
                    ClientId = "webapi.m2m.client",
                    ClientName = "WebApi Credentials Client",

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256()) },

                    AllowedScopes = { "emailsending.scope" }
                },

                new Client()
                {
                    ClientId = "webapi.swaggerui",
                    ClientName = "WebApi Swagger UI",

                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,
                    RequirePkce = false,
                    AllowAccessTokensViaBrowser = true,
                   
                    RedirectUris = {$"{externalUrls.WebApi}/swagger/oauth2-redirect.html"},
                    PostLogoutRedirectUris = {$"{externalUrls.WebApi}/swagger/"},

                    AllowedCorsOrigins = {$"{externalUrls.WebApi}"},
                    AllowedScopes = {"openid", "profile", "exam.scope" }
                },

                // interactive client using code flow + pkce
                new Client
                {
                    ClientId = "webapi.interactive",
                    ClientSecrets = { new Secret("49C1A7E1-0C79-4A89-A3D6-A37998FB86B0".Sha256()) },

                    AllowedGrantTypes = GrantTypes.Code,

                    RedirectUris = { $"{externalUrls.WebApi}/signin-oidc" },
                    FrontChannelLogoutUri = $"{externalUrls.WebApi}/signout-oidc",
                    PostLogoutRedirectUris = { $"{externalUrls.WebApi}/signout-callback-oidc" },

                    AllowOfflineAccess = true,
                    AllowedScopes = { "openid", "profile", "exam.scope" }
                },
            };
    }
}