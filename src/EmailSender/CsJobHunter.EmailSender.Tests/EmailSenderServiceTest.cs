using CsJobHunter.EmailSender.Dtos;
using CsJobHunter.EmailSender.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace CsJobHunter.EmailSender.Tests
{
    public class EmailSenderServiceTest
    {
        [Theory]
        [InlineData(0)]
        [InlineData(17)]
        [InlineData(51)]
        [InlineData(100)]
                
        public async void SendAsync_FakeSendToSetOfAddresses_Succeedes(int emailAddressesNum)
        {
            // Arrange
            EmailDto emailDto = new()
            {
                Sender = "ateam_csjobhunter@mail.ru",
                Subject = "Invitation",
                Body = "We invite you to take the exam on C#",
                Addresses = new List<string>()
            };

            for (int i = 0; i < emailAddressesNum; i++)
                emailDto.Addresses.Add("user_" + i + "@a_yandex.ru");

            // Instead of FakeEmailSenderByAddressesForTest, we could take Mock<IEmailSenderByAddresses>,
            // but then we could only look at the number of addresses received, without checking the uniqueness of each address.
            var fakeEmailSenderByAddresses = new FakeEmailSenderByAddressesForTest();
            var emailSenderService = new EmailSenderService(fakeEmailSenderByAddresses);
            
            // Act
            await emailSenderService.SendAsync(emailDto );

            // Assert   
            
            // If the number matches and there is every line in the resulting/sent list -> the lists match.
            Assert.Equal(emailDto.Addresses.Count, fakeEmailSenderByAddresses.sendedEmailsImitation.Count);
            foreach (var addr in emailDto.Addresses)
            {
                string emailImitationForAddr = MailToStr(addr, emailDto.Sender, emailDto.Subject, emailDto.Body);
                Assert.True(fakeEmailSenderByAddresses.IsContainInBag(emailImitationForAddr));
            }
        }

        [Fact]
        public async void SendAsync_SendToRealAddresses_Succeedes()
        {
            // Arrange
            EmailDto emailDto = new()
            {
                Sender = "ateam_csjobhunter@mail.ru",
                Subject = "Invitation",
                Body = "We invite you to take the exam on C#",
                Addresses = new List<string>()
                {
                    "cs-serge1@yandex.ru",
                    "sergei@vniigis-ztk.ru"
                }
            };

            var emailSenderService = new EmailSenderService(new EmailSenderByAddresses());

            try
            {
                // Act                
                await emailSenderService.SendAsync(emailDto);
            }
            catch (Exception)
            {
                // Assert
                Assert.True(false);
            }
        }

        public static string MailToStr(string address, string sender, string subject, string body)
        {
            return $"address: {address}   sender: {sender}   subject: {subject}  body: {body} ";
        }
    }
    
    // Fills in sendedEmailsImitation, instead of sending emails.
    public class FakeEmailSenderByAddressesForTest : IEmailSenderByAddresses
    {
        public ConcurrentBag<string> sendedEmailsImitation;

        public FakeEmailSenderByAddressesForTest()
        {
            sendedEmailsImitation = new();
        }

        public void SendToTheFollowingAddresses(IEnumerable<string> addresses, string sender, string subject, string body)
        {
            foreach (var addr in addresses)
                sendedEmailsImitation.Add(EmailSenderServiceTest.MailToStr(addr, sender, subject, body));
        }

        public bool IsContainInBag(string emailImitation)
        {
            foreach (var str in sendedEmailsImitation)
                if (str == emailImitation)
                    return true;

            return false;
        }
    }
}
