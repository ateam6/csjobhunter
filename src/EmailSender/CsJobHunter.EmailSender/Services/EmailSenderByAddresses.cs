﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CsJobHunter.EmailSender.Services
{
    public class EmailSenderByAddresses : IEmailSenderByAddresses
    {        
        public void SendToTheFollowingAddresses(IEnumerable<string> addresses, string sender, string subject, string body)
        {
            Task taskSend = Task.Run(async () =>
            {
                using SmtpClient smtpClient = new("smtp.mail.ru", 25)
                {
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = GetCredentialForSender(sender)
                };

                foreach (var address in addresses)
                {                    
                    await smtpClient.SendMailAsync(sender, address, subject, body);
                }
            });
            try
            {
                taskSend.Wait(); // Wait until the above task is complete, email is sent.

            }
            catch (Exception e)
            {
                //TODO some exception logging
            }
        }        

        private static NetworkCredential GetCredentialForSender(string sender)
        {
            return new NetworkCredential("ateam_csjobhunter@mail.ru", "fP-jsTOIpu13");
        }
    }
}
