﻿using CsJobHunter.EmailSender.Dtos;
using System.Threading.Tasks;

namespace CsJobHunter.EmailSender.Services
{
    public interface IEmailSenderService
    {
        public Task SendAsync(EmailDto emailDto);
    }
}
