﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CsJobHunter.EmailSender.Services
{
    public interface IEmailSenderByAddresses
    {
        void SendToTheFollowingAddresses(IEnumerable<string> addresses, string sender, string subject, string body);
    }
}
