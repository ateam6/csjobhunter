﻿using CsJobHunter.EmailSender.Dtos;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CsJobHunter.EmailSender.Services
{
    public class EmailSenderService : IEmailSenderService
    {
        private int _threadsNum = Environment.ProcessorCount;

        // Т.к. потоками используется только для чтения, использую как обычную переменную.
        private EmailDto _emailDto;
        private readonly IEmailSenderByAddresses _senderByAddresses;

        private int _workingThreadsNum;
        private int _addressesNumForThread;

        public EmailSenderService(IEmailSenderByAddresses senderByAddresses)
        {
            _senderByAddresses = senderByAddresses;
        }

        public async Task SendAsync(EmailDto emailDto)
        {
            // Т.к. потоками используется только для чтения, использую как обычную переменную.
            _emailDto = emailDto ?? throw new ArgumentException("emailDto == null in EmailSenderService.SendAsync call");

            _addressesNumForThread = _emailDto.Addresses.Count / _threadsNum;

            _workingThreadsNum = _threadsNum;

            for (int thrN = 0; thrN < _threadsNum; thrN++)
                ThreadPool.QueueUserWorkItem(TakeAChunkOfAddressesAndSend, thrN);

            // Ждём завершения всех потоков
            while (_workingThreadsNum > 0)
                Thread.Sleep(0);
        }

        private void TakeAChunkOfAddressesAndSend(object thrdNum)
        {
            var (beginningIndexForThread, upToIndexForThread) = GetBeginAndFinishIndexesForThreadNum((int)thrdNum);

            var addressesChunk = _emailDto.Addresses.Skip(beginningIndexForThread).Take(upToIndexForThread - beginningIndexForThread);

            _senderByAddresses.SendToTheFollowingAddresses(addressesChunk, _emailDto.Sender, _emailDto.Subject, _emailDto.Body);

            Interlocked.Decrement(ref _workingThreadsNum);
        }

        // Для потока с номером threadN разослать по адресам c индексами [beginningIndexForThread,upToIndexForThread) в EmailDto.Addresses.
        private (int beginningIndexForThread, int upToIndexForThread) GetBeginAndFinishIndexesForThreadNum(int threadN)
        {
            if (threadN == _threadsNum - 1)
                return (threadN * _addressesNumForThread, _emailDto.Addresses.Count);
            else
                return (threadN * _addressesNumForThread, (threadN + 1) * _addressesNumForThread);
        }
    }
}
