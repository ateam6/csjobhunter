﻿using CsJobHunter.EmailSender.Dtos;
using CsJobHunter.EmailSender.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CsJobHunter.EmailSender.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmailSenderController : ControllerBase
    {
        private readonly ILogger<EmailSenderController> _logger;
        private readonly IEmailSenderService _emailSenderService;

        public EmailSenderController(
            ILogger<EmailSenderController> logger,
            IEmailSenderService emailSenderService)
        {
            _logger = logger;
            _emailSenderService = emailSenderService;
        }

        [HttpPost]
        public async Task<IActionResult> SendAsync(EmailDto emailDto)
        {
            await _emailSenderService.SendAsync(emailDto);

            return Ok();
        }
    }
}
