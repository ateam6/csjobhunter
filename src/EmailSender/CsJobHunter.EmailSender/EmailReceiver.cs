﻿using CsJobHunter.EmailSender.Dtos;
using CsJobHunter.EmailSender.Options;
using CsJobHunter.EmailSender.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace CsJobHunter.EmailSender
{
    public class EmailReceiver : BackgroundService
    {
        private readonly string _queue;

        private readonly IEmailSenderService _emailSenderService;

        private readonly IModel _channel;

        public EmailReceiver(IOptions<RabbitMqOptions> rabbitMqOptions, IEmailSenderService emailSenderService)
        {
            _queue = rabbitMqOptions.Value.Queue;

            var factory = new ConnectionFactory()
            {
                HostName = rabbitMqOptions.Value.HostName,
                VirtualHost = "/",
                UserName = rabbitMqOptions.Value.UserName,
                Password = rabbitMqOptions.Value.Password,
                Port = rabbitMqOptions.Value.Port
            };

            var connection = factory.CreateConnection();
            _channel = connection.CreateModel();

            _emailSenderService = emailSenderService;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _channel.QueueDeclare(queue: _queue,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                var emailDto = JsonSerializer.Deserialize<EmailDto>(message.ToString());

                _emailSenderService.SendAsync(emailDto);
            };
            _channel.BasicConsume(queue: _queue,
                autoAck: true,
                consumer: consumer);

            return Task.CompletedTask;
        }
    }
}
